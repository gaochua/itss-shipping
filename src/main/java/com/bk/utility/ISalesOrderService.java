package com.bk.utility;

import java.util.List;

import com.bk.model.SalesOrder;


public interface ISalesOrderService {

	public List<SalesOrder> getList();
	
	public int createSalesOrder(SalesOrder salesOrder);
	
	public SalesOrder getById(int orderId);
	
	public void deleteSalesOrder(SalesOrder salesOrder);
	
}
