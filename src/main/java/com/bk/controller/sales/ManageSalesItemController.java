package com.bk.controller.sales;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.bk.model.SalesOrderItem;
import com.bk.utility.ISalesItemService;
import com.bk.utility.SalesItemServiceImp;
import com.bk.utility.SalesItemTableModel;
import com.bk.utility.SearchEvent;
import com.bk.view.sales.SalesItemForm;



public class ManageSalesItemController {
	private JPanel pnlView;
	private JTextField tfSearch;
	
	private JButton btnAdd;

	
	private ISalesItemService salesItemService = null;
	private final String[] listColumn = {"ID", "Merchandise Code", "Product Name", "Quantity", "Unit", "Desired Date"};
	
	private TableRowSorter<TableModel>  rowSorter = null;	
	  
	public ManageSalesItemController(JPanel pnlView, JTextField tfSearch, JButton btnAdd) {
		super();
		this.pnlView = pnlView;
		this.tfSearch = tfSearch;
		this.btnAdd = btnAdd;
		this.salesItemService = new SalesItemServiceImp();
	}
	
	public void closeMainSalesItemFrame() {
		JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(pnlView);
		frame.setVisible(false);
		frame.dispose();
	}
	
	public void viewSalesItem(int orderId) {
		List <SalesOrderItem> listItem = salesItemService.getList(orderId);
		
		DefaultTableModel model = new SalesItemTableModel().setTableSalesItem(listItem, listColumn);
		JTable table = new JTable(model);
		
		rowSorter = new TableRowSorter<TableModel>(table.getModel());
		table.setRowSorter(rowSorter);
		
		//add Search Event
		SearchEvent searchEvent = new SearchEvent();
		searchEvent.setSearchEvent(tfSearch, rowSorter);

		//add event table
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				if (e.getClickCount() == 2 && table.getSelectedRow() != -1) {
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					int selectedRowIndex = table.getSelectedRow();
					
					SalesOrderItem salesItem = listItem.get(selectedRowIndex);
					
					SalesItemForm form = new SalesItemForm(salesItem, orderId);
					form.setLocationRelativeTo(null);
					form.setResizable(false);
					form.setTitle("Sales Item Information");
					form.setVisible(true);
					
//					closeMainSalesItemFrame();

				}
			}
		});  		

		
		// design
        table.getTableHeader().setFont(new Font("Arial", Font.BOLD, 14));
        table.getTableHeader().setPreferredSize(new Dimension(100, 50));
        table.setRowHeight(50);
        table.validate();
        table.repaint();
        
        JScrollPane scroll = new JScrollPane();
        scroll.getViewport().add(table);
        scroll.setPreferredSize(new Dimension(1350, 400));
        pnlView.removeAll();
        pnlView.setLayout(new CardLayout());
        pnlView.add(scroll);
        pnlView.validate();
        pnlView.repaint();
		
	}
	
	public void createSalesItem(int orderId) {
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				SalesItemForm form = new SalesItemForm(new SalesOrderItem(), orderId);
				form.setTitle("Sales Item Information");
				form.setLocationRelativeTo(null);
				form.setResizable(false);
				form.setVisible(true);
				
//				closeMainSalesItemFrame();
			};
		});
	}


}
