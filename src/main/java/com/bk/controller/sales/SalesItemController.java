package com.bk.controller.sales;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.bk.utility.UIHelper;
import com.bk.model.Product;
import com.bk.model.SalesOrder;
import com.bk.model.SalesOrderItem;
import com.bk.model.Unit;
import com.bk.utility.IProductService;
import com.bk.utility.ISalesItemService;
import com.bk.utility.ISalesOrderService;
import com.bk.utility.ProductServiceImp;
import com.bk.utility.SalesItemServiceImp;
import com.bk.utility.SalesOrderServiceImp;
import com.toedter.calendar.JDateChooser;

public class SalesItemController {
	
	private JButton btnSave;
	private JButton btnDelete;
	private JTextField tfSalesItemId;
	private JTextField tfQuantity;
	private JComboBox cbUnit;
	
//	private JTextField tfProduct;
	private JComboBox cbProduct = new JComboBox();
	private JDateChooser dateChooser = new JDateChooser();
	
	
	private JLabel lblMessage;
	
	private int orderId;
	
	private ISalesItemService salesItemService = null; 
	
	
	
	public SalesItemController(JTextField tfSalesItemId, JTextField tfQuantity, JComboBox cbProduct, JComboBox cbUnit, JDateChooser dateChooser,
			JLabel lblMessage, JButton btnSave, JButton btnDelete) {
		super();
		this.btnSave = btnSave;
		this.btnDelete = btnDelete;
		this.tfSalesItemId = tfSalesItemId;
//		this.tfProduct = tfProduct;
		this.tfQuantity = tfQuantity;
		this.cbUnit = cbUnit;
		this.cbProduct = cbProduct;
		this.dateChooser = dateChooser;
		this.salesItemService = new SalesItemServiceImp();

		this.lblMessage = lblMessage;
	}
	
	public void setView(SalesOrderItem salesItem) {
		IProductService productService= new ProductServiceImp();
		List<Product> listProduct = productService.getList();
		Vector<Product> productVector = new Vector<>(listProduct);

		if (salesItem.getId() != 0) {
			tfSalesItemId.setText(Integer.toString(salesItem.getId()));
			tfQuantity.setText(Integer.toString(salesItem.getQuantity()));
			cbUnit.setSelectedItem(salesItem.getUnit());
//			tfProduct.setText(Integer.toString(salesItem.getProduct().getId()));

			for (Product product : productVector) {
//				System.out.println(product);
				if (product.equals(salesItem.getProduct())) {
					cbProduct.setSelectedItem(product);
					break;
				}
			}
			
			dateChooser.setDate(salesItem.getDesiredDate());
		}
	}
	
	public void setEvent(SalesOrderItem salesItem, int orderId) {
		btnSave.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				try {
					if (!checkNotNull()) {
						lblMessage.setText("Please enter required field!");
					} else if (tfSalesItemId.getText().length() == 0){
						//							//create
						readDataFromForm(salesItem);
						//							salesItem = new SalesOrderItem();

						ISalesOrderService orderService = new SalesOrderServiceImp();
						SalesOrder order = orderService.getById(orderId);						
						salesItem.setOrder(order);

						salesItemService.getList(orderId).add(salesItem);
						salesItemService.create(salesItem);
//						lblMessage.setText("Create Successfully!");
						UIHelper.showSuccessDialog("Create Successfully!");

						
					} else {
						//update
						readDataFromForm(salesItem);
						salesItemService.update(salesItem);
//						lblMessage.setText("Update Successfully!");
						UIHelper.showSuccessDialog("Update Successfully!");

					}
				} catch (Exception ex) {
                    lblMessage.setText(ex.toString());
//                    System.out.println(ex.toString());
                }

			};
		});
		
		btnDelete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
//				//delete
				salesItemService.getList(orderId).remove(salesItem);
				salesItemService.delete(salesItem);
//				lblMessage.setText("Delete Successfully!");
				UIHelper.showSuccessDialog("Delete Successfully!");


			}
		});
		
	}
	
	private boolean checkNotNull() {
		return tfQuantity.getText().length() != 0 && cbProduct.getSelectedItem() != null 
				&& dateChooser.getDate() != null && cbUnit.getSelectedItem() != null;
	}
	
	private void readDataFromForm(SalesOrderItem salesItem) {
		
		Product product = (Product) cbProduct.getSelectedItem();
		salesItem.setProduct(product);
		salesItem.setQuantity(Integer.parseInt(tfQuantity.getText()));
		salesItem.setUnit((Unit) cbUnit.getSelectedItem());
		Date date = dateChooser.getDate();
		salesItem.setDesiredDate(date);
	}
	

}
