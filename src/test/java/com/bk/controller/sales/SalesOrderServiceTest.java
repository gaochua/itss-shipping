package com.bk.controller.sales;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Test;

import com.bk.model.SalesOrder;
import com.bk.model.User;
import com.bk.utility.HibernateUtil;
import com.bk.utility.ISalesOrderService;
import com.bk.utility.SalesOrderServiceImp;

public class SalesOrderServiceTest {

	@Test
	public void testGetList() {
		ISalesOrderService salesService = new SalesOrderServiceImp();
		List<SalesOrder> list = salesService.getList();
		
		assertTrue("No Sale Order found", list.size() > 0);;
	}


	@Test
	public void testGetById() {
		ISalesOrderService salesService = new SalesOrderServiceImp();
		List<SalesOrder> list = salesService.getList();
		
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(list.size()) + 1;
		SalesOrder order = salesService.getById(randomInt);
		
		assertNotNull("No Sale Order with given ID found", order);
	}

}
