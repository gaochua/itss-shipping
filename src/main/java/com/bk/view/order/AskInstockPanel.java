package com.bk.view.order;

import com.bk.App;
import com.bk.model.ImportSite;
import com.bk.model.Product;
import com.bk.model.SalesOrder;
import com.bk.model.SalesOrderItem;
import com.bk.model.SiteInstock;

import jiconfont.icons.font_awesome.FontAwesome;
import jiconfont.swing.IconFontSwing;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.table.DefaultTableModel;

public class AskInstockPanel extends JPanel {
    //private JButton backBtn = new JButton("Cancel");
    private JButton viewBtn = new JButton("View in-stock quantity");
    private JButton submitBtn = new JButton("Exit");
    private JScrollPane scrollPane = new JScrollPane();
    private JPanel childPanel = new JPanel();
    private JTable table;
    private DefaultTableModel tableModel;
    private SalesOrder salesOrder;
    private List<ImportSite> allSites;
    private List<ImportSite> suitableSites; //Sites get at least 1 order product

    // public JButton getBackBtn() {
    //     return backBtn;
    // }

    public JButton getViewBtn() {
        return viewBtn;
    }

    public JButton getSubmitBtn() {
        return submitBtn;
    }
  
    public void setAllSites(List<ImportSite> allSites) {
        this.allSites = allSites;
    }

    public List<ImportSite> getAllSites() {
        return this.allSites;
    }

    public void setSalesOrder(SalesOrder salesOrder) {
        this.salesOrder = salesOrder;
    }

    public SalesOrder getSalesOrder() {
        return this.salesOrder;
    }

    private Set<Product> getOrderProducts() {
        Set<SalesOrderItem> orderItems = getSalesOrder().getItems();
        Set<Product> orderProducts = new HashSet<Product>();
        for(SalesOrderItem orderItem: orderItems) {
            orderProducts.add(orderItem.getProduct());
        }
        return orderProducts;
    }

    public void setSuitableSites() {
        suitableSites = new ArrayList<ImportSite>();
        Set<Product> orderProducts = getOrderProducts();
        for(ImportSite site: allSites) {
            Set<SiteInstock> instocks = site.getInstocks();
            if(!suitableSites.contains(site)) {
                for(SiteInstock instock: instocks) {
                    if(orderProducts.contains(instock.getProduct())) {
                        suitableSites.add(site);
                        break;
                    }
                }
            }
        }
        showData();
    }

    public List<ImportSite> getSuitableSites() {
        return suitableSites;
    }

    public List<SiteInstock> getInstockFromSite(ImportSite site) {
        List<SiteInstock> instocks = new ArrayList<SiteInstock>();
        Set<Product> orderProducts = getOrderProducts();
        for(SiteInstock instock: site.getInstocks()) {
            if(orderProducts.contains(instock.getProduct())) {
                instocks.add(instock);
            }
        }
        return instocks;
    }

    public void showData() {
        childPanel.setLayout(new FlowLayout());
        
        remove(scrollPane);
        remove(childPanel);

        if(suitableSites == null) {
            JTextField text = new JTextField("Not exists sites have order products");
            scrollPane = new JScrollPane(text);
            add(scrollPane, BorderLayout.CENTER);
            childPanel.add(submitBtn);
            add(childPanel, BorderLayout.PAGE_END);
        }

        else {
            String[] columnNames = {"ID", "Name", "Product"};
            String[][] values = new String[suitableSites.size()][3];
            
            for (int i=0; i<this.suitableSites.size(); i++) {
                ImportSite site = this.suitableSites.get(i);
                values[i][0] = Integer.toString(site.getId());
                values[i][1] = site.getName();
                values[i][2] = "";
                List<SiteInstock> instocks = getInstockFromSite(site);
                for(SiteInstock instock: instocks) {
                    values[i][2] += instock.getProduct().getName() + ", ";
                }
                
            }
            tableModel = new DefaultTableModel(values, columnNames);
            table = new JTable(tableModel);
            table.setRowSelectionAllowed(true);
            table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            // table.setFont(new Font("Serif", Font.BOLD, 16));

            scrollPane = new JScrollPane(table);

            add(scrollPane, BorderLayout.CENTER);
            //childPanel.add(backBtn);
            childPanel.add(viewBtn);
            childPanel.add(submitBtn);
        
            add(childPanel, BorderLayout.PAGE_END);
        } 
        
    }

    private void setupButtons() {
        Icon nextIcon = IconFontSwing.buildIcon(FontAwesome.FORWARD, 16);
        //Icon backIcon = IconFontSwing.buildIcon(FontAwesome.BACKWARD, 16);
        submitBtn.setIcon(nextIcon);
        //backBtn.setIcon(backIcon);
    }

    public AskInstockPanel() {
        setLayout(new BorderLayout());
        setupButtons();
    }

    public List<ImportSite> getSelectedSites() {
        if(suitableSites == null) return null;
        int index[] = table.getSelectedRows();
        if(index == null) {
            return null;
        }
        List<ImportSite> selectedSites = new ArrayList<ImportSite>();
        for(int i: index) {
            selectedSites.add(this.suitableSites.get(i));
        }

        return selectedSites;
    }
   
}
