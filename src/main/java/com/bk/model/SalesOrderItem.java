package com.bk.model;



import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "sales_order_item")
public class SalesOrderItem implements DataModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name="product_id", nullable=false)
    private Product product;

    @Column(nullable = false)
    private int quantity;
    @Column(nullable = false)
    private Unit unit;
    @Column(nullable = false)
//    private Date desiredDate = Date.valueOf("2019-12-19");
    private Date desiredDate;


    @ManyToOne
    @JoinColumn(name="order_id", nullable=false)
    private SalesOrder order;

    public SalesOrderItem(Product product, int quantity, Unit unit, SalesOrder order, Date desiredDate) {
        this.product = product;
        this.quantity = quantity;
        this.unit = unit;
        this.order = order;
        this.desiredDate = desiredDate;
    }

    public SalesOrderItem(Product product, int quantity, Unit unit, SalesOrder order) {
        this.product = product;
        this.quantity = quantity;
        this.unit = unit;
        this.order = order;
        this.desiredDate = new Date();
    }

    public SalesOrderItem() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public SalesOrder getOrder() {
        return order;
    }

    public void setOrder(SalesOrder order) {
        this.order = order;
    }

    public Date getDesiredDate() {
        return desiredDate;
    }

    public void setDesiredDate(Date desiredDate) {
        this.desiredDate = desiredDate;
    }
}
