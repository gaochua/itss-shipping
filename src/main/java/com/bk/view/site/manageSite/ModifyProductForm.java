package com.bk.view.site.manageSite;

import java.util.List;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.bk.model.SiteInstock;
import com.bk.model.Unit;
import com.bk.utility.HibernateUtil;
import com.bk.utility.IntegerFilterDocument;

@SuppressWarnings({"serial", "unchecked"})
public class ModifyProductForm extends FillableForm {
		
	public ModifyProductForm(SiteInstock instock) {
		super();		
		JComboBox<String> products = get(1);
		List<String> productList = HibernateUtil.get(session -> session.createQuery("Select name From Product").getResultList());
		Vector<String> productNameVector = new Vector<String>(productList);
		productNameVector.add(0, "Add new product");
		for (String s : productNameVector) products.addItem(s);
		products.setSelectedIndex(productNameVector.indexOf(instock.getProduct().getName()));
		
		JComboBox<Unit> units = get(7);
		units.setSelectedItem(instock.getUnit());
		
		JTextField quantity = get(9);
		quantity.setDocument(new IntegerFilterDocument());
		quantity.setText(Integer.toString(instock.getQuantity()));
	}

	@Override
	public void layoutContent() {
		JLabel productNameLabel = new JLabel("Product name:");
		JComboBox<String> products = new JComboBox<String>();
		JLabel newProductNameLabel = new JLabel("New product's name:");
		JTextField newProductName = new JTextField();
		JLabel productDescriptionLabel = new JLabel("New product's description:");
		JTextArea description = new JTextArea();
		JLabel productUnitLabel = new JLabel("Unit:");
		JComboBox<Unit> units = new JComboBox<Unit>(Unit.values());
		JLabel productQuantityLabel = new JLabel("Quantity:");
		JTextField quantity = new JTextField();
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(productNameLabel);
		add(products);
		add(newProductNameLabel);
		add(newProductName);
		add(productDescriptionLabel);
		add(description);
		add(productUnitLabel);
		add(units);
		add(productQuantityLabel);
		add(quantity);
	}

	@Override
	public <E> E getContent(int index) {
		// TODO Auto-generated method stub
		if (index % 2 == 0) {
			return (E) ((JLabel) get(index)).getText();
		}
		else {
			switch (index) {
			case 1:
				return (E) ((JComboBox<String>) get(index)).getSelectedItem();
			case 3:
				return (E) ((JTextField) get(index)).getText();
			case 5:
				return (E) ((JTextArea) get(index)).getText();
			case 7:
				return (E) ((JComboBox<Unit>) get(index)).getSelectedItem();
			default://9
				return (E) ((Integer) Integer.parseInt(((JTextField) get(index)).getText()));
			}
		}
	}

}
