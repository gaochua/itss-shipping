package com.bk.controller.order;

import com.bk.App;
import com.bk.utility.HibernateUtil;
import com.bk.model.DeliveryOrder;
import com.bk.model.ImportSite;
import com.bk.model.Product;
import com.bk.model.SalesOrder;
import com.bk.model.SalesOrderItem;
import com.bk.model.SiteInstock;
import com.bk.view.main.AbstractHomePanel;
import com.bk.view.order.OverseasOrderPanel;

import java.util.List;

public class OverseasOrderController {
    private AbstractHomePanel homePanel;
    private OverseasOrderPanel orderPanel;
    private ManageDeliveryController manageDeliveryController;
    private InstockController instockController;


    public OverseasOrderController(AbstractHomePanel homePanel, OverseasOrderPanel orderPanel) {
        this.homePanel = homePanel;
        this.orderPanel = orderPanel;
        manageDeliveryController = new ManageDeliveryController(orderPanel);
        instockController = new InstockController(orderPanel);
        init();
    }

    private void init() {
        orderPanel.getBackBtn().addActionListener(actionEvent -> App.mainFrame.replacePanel(orderPanel, homePanel));
        orderPanel.getDeliveryBtn().addActionListener(actionEvent -> {
            initDelivery();
        });
        orderPanel.getInstockBtn().addActionListener(actionEvent -> {
            initInstock();
        });
    }

    private void initDelivery() {
        List<DeliveryOrder> deliveryOrders = HibernateUtil.get(session -> session.createQuery("From DeliveryOrder").list());
        List<Product> products = HibernateUtil.get(session -> session.createQuery("From Product").list());
        List<ImportSite> sites = HibernateUtil.get(session -> session.createQuery("From ImportSite").list());

        manageDeliveryController.init(deliveryOrders, products, sites);
    }

    private void initInstock() {
        // DbHelper.populateDb();
        List<SalesOrder> salesOrders = HibernateUtil.get(session -> session.createQuery("From SalesOrder").list());
        List<ImportSite> sites = HibernateUtil.get(session -> session.createQuery("From ImportSite").list());
        List<SiteInstock> instocks = HibernateUtil.get(session -> session.createQuery("From SiteInstock").list());
        List<Product> products = HibernateUtil.get(session -> session.createQuery("From Product").list());
        List<DeliveryOrder> orders = HibernateUtil.get(session -> session.createQuery("From DeliveryOrder").list());
        List<SalesOrderItem> items = HibernateUtil.get(session -> session.createQuery("From SalesOrderItem").list());
        
        instockController.init(salesOrders, sites, instocks, products, orders, items);
    }
}
