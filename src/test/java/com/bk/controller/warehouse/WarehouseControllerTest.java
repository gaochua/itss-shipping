package com.bk.controller.warehouse;

import static org.junit.Assert.*;

import javax.swing.JTextField;

import org.junit.Test;

import com.bk.model.DeliveryOrder;
import com.bk.utility.IWarehouseService;
import com.bk.utility.UIHelper;
import com.bk.utility.WarehouseServiceImp;

public class WarehouseControllerTest {

	@Test
	public void testSetEventCheck() {
		IWarehouseService warehouseService = new WarehouseServiceImp();
		DeliveryOrder deliveryOrder = warehouseService.getProcessingList().get(0);
		
		JTextField tfQuantityReceived = new JTextField();
		
		//Test cases
//		tfQuantityReceived.setText(null);
		tfQuantityReceived.setText("5");
//		tfQuantityReceived.setText("2.5");
//		tfQuantityReceived.setText("abc");
		//End of test cases
		
		
		try {
			if (tfQuantityReceived.getText().length() == 0) {
				System.out.println("Please enter required field!");
			}else {
				deliveryOrder.setChecked(true);
				deliveryOrder.setQuantityReceived(Integer.parseInt(tfQuantityReceived.getText()));
				UIHelper.showSuccessDialog("Update Successfully!");
			}
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}

		assertTrue("Fail to enter quantity received", deliveryOrder.getQuantityReceived() != 0);
	}

}
