package com.bk.controller.warehouse;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.bk.utility.UIHelper;
import com.bk.model.DeliveryOrder;
import com.bk.utility.IWarehouseService;
import com.bk.utility.WarehouseServiceImp;

public class WarehouseController {
	
	private JTextField tfDeliveryOrderId;
	private JTextField tfProduct;
	private JTextField tfQuantityOrder;
	private JTextField tfQuantityReceived;

	private JComboBox cbUnit;
	
	private JButton btnSave;
	
	private JLabel lblMessage;
	
	private IWarehouseService warehouseService = null;

	public WarehouseController(JTextField tfDeliveryOrderId, JTextField tfProduct, JTextField tfQuantityOrder,
			JTextField tfQuantityReceived, JComboBox cbUnit, JButton btnSave, JLabel lblMessage) {
		super();
		this.tfDeliveryOrderId = tfDeliveryOrderId;
		this.tfProduct = tfProduct;
		this.tfQuantityOrder = tfQuantityOrder;
		this.tfQuantityReceived = tfQuantityReceived;
		this.cbUnit = cbUnit;
		this.btnSave = btnSave;
		this.lblMessage = lblMessage;
		this.warehouseService = new WarehouseServiceImp();
	}

	public void setEventView(DeliveryOrder deliveryOrder) {
		tfDeliveryOrderId.setText(Integer.toString(deliveryOrder.getId()));
		tfProduct.setText(deliveryOrder.getProduct().getName());
		tfQuantityOrder.setText(Integer.toString(deliveryOrder.getQuantity()));
		
		cbUnit.setSelectedItem(deliveryOrder.getUnit());
	}

	public void setEventCheck(DeliveryOrder deliveryOrder) {
		btnSave.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				try {
					if (!checkNotNull()) {
						lblMessage.setText("Please enter required field!");
					}else {
						deliveryOrder.setChecked(true);
						deliveryOrder.setQuantityReceived(Integer.parseInt(tfQuantityReceived.getText()));
						warehouseService.update(deliveryOrder);
//						lblMessage.setText("Update Successfully!");
						UIHelper.showSuccessDialog("Update Successfully!");
					}
				} catch (Exception ex) {
					lblMessage.setText(ex.toString());
				}

			};
		});
	}
	
	private boolean checkNotNull() {
		return tfQuantityReceived.getText().length() != 0;
	}


}
