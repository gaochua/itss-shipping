package com.bk.model;


import javax.persistence.*;

@Entity
@Table(name = "site_instock")
public class SiteInstock implements DataModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name="product_id", nullable=false)
    private Product product;

    @Column(nullable = false)
    private int quantity;
    @Column(nullable = false)
    private Unit unit;

    @ManyToOne
    @JoinColumn(name="site_id", nullable=false)
    private ImportSite site;

    public SiteInstock(Product product, int quantity, Unit unit, ImportSite site) {
        this.product = product;
        this.quantity = quantity;
        this.unit = unit;
        this.site = site;
    }

    public SiteInstock() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public ImportSite getSite() {
        return site;
    }

    public void setSite(ImportSite site) {
        this.site = site;
    }
    
    @Override
    public boolean equals(Object obj) {
    	return id == ((SiteInstock) obj).getId();
    }
}
