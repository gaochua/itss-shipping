package com.bk.controller.main;

import javax.swing.JPanel;

import com.bk.view.main.AbstractHomePanel;
import com.bk.view.order.OverseasOrderPanel;
import com.bk.view.sales.SalesPanel;
import com.bk.view.site.manageSite.SitePanel;

public abstract class AbstractHomeController extends AbstractController{
    protected AbstractHomePanel homePanel;
    protected JPanel orderPanel;
    protected JPanel salesPanel;
    protected JPanel sitePanel;

    public AbstractHomeController(AbstractHomePanel homePanel, JPanel orderPanel,
                                  JPanel salesPanel, JPanel sitePanel) {
        this.homePanel = homePanel;
        this.orderPanel = orderPanel;
        this.salesPanel = salesPanel;
        this.sitePanel = sitePanel;
        setupPanels();
    }

}
