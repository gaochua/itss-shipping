package com.bk.utility;

import java.util.List;

import com.bk.model.SalesOrderItem;
import com.bk.model.Unit;

public interface ISalesItemService {
	
	public List<SalesOrderItem> getList(int orderId);
	
	public void create(SalesOrderItem salesItem);
	
	public void update(SalesOrderItem salesItem);
	
	public void delete(SalesOrderItem salesItem);

}
