package com.bk.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "user")
public class User implements DataModel{
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private Role role;

    @ManyToOne
    @JoinColumn(name="site_id", nullable=true)
    private ImportSite site;


    public User(String name, String password, Role role) {
        this.name = name;
        this.password = password;
        this.role = role;
    }

    public User() {}

    @OneToMany(mappedBy="user")
    private Set<SalesOrder> salesOrders;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public ImportSite getSite() {
		return site;
	}

	public void setSite(ImportSite site) {
		this.site = site;
	}

	@Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", role=" + role +
                '}';
    }
}
