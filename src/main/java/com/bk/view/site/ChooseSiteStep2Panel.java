package com.bk.view.site;

import com.bk.App;
import com.bk.controller.SortByQuantity;
import com.bk.model.DeliveryMethod;
import com.bk.model.DeliveryOrder;
import com.bk.model.ImportSite;
import com.bk.model.Product;
import com.bk.model.SalesOrder;
import com.bk.model.SalesOrderItem;
import com.bk.model.SiteInstock;

import jiconfont.icons.font_awesome.FontAwesome;
import jiconfont.swing.IconFontSwing;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.awt.*;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class ChooseSiteStep2Panel extends JPanel {
    private JButton backBtn = new JButton("Cancel");
    private JButton submitBtn = new JButton("Submit");
    
    //private JScrollPane scrollPane = new JScrollPane();
    private JPanel childPanel = new JPanel();
    private JPanel orderBox, contentBox;
    private JTable table;
    private DefaultTableModel tableModel;
    
    private List<SalesOrderItem> salesOrderItems;
    private SalesOrder salesOrder;
    private List<ImportSite> allSites;
    private List<ImportSite> suitableSites;
    private List<ImportSite> deliveryShipSites;
    private List<ImportSite> deliveryAirSites;
    private List<DeliveryOrder> orders;
    private List<DeliveryOrder> allOrders;

    public void setSalesOrder(SalesOrder salesOrder) {
        this.salesOrder = salesOrder;
    }

    public void setSalesOrderItems() {
        salesOrderItems = new ArrayList<SalesOrderItem>();
        for(SalesOrderItem item: salesOrder.getItems()) {
            salesOrderItems.add(item);
        }
    }

    public List<SalesOrderItem> getSalesOrderItems() {
        return salesOrderItems;
    }

    public void setAllSites(List<ImportSite> allSites) {
        this.allSites = allSites;
    }

    private long betweenDates(Date d1, Date d2) throws IOException{
        long diffInMillies = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    public void setSuitableSites(SalesOrderItem salesOrderItem) throws IOException {
        suitableSites = new ArrayList<ImportSite>();
        Date currentDate = new Date(System.currentTimeMillis());
        for(ImportSite site: allSites) {
            int diff = (int) betweenDates(currentDate, salesOrderItem.getDesiredDate());
            if(diff >= Math.max(site.getDays_delivery_air(), site.getDays_delivery_ship())) {
                Set<SiteInstock> instocks = site.getInstocks();
                for(SiteInstock instock: instocks) {
                    if(instock.getProduct().equals(salesOrderItem.getProduct())) {
                        suitableSites.add(site);
                    }
                }
            }
            
        }
        
    }

    private boolean checkFirstCondition(ImportSite site) {
        if(Math.min(site.getDays_delivery_air(), site.getDays_delivery_ship()) == site.getDays_delivery_air())
            return true;
        return false;
    }

    private List<ImportSite> sortSitesByQuantity(List<ImportSite> sites, SalesOrderItem salesOrderItem) {
        Collections.sort(sites, new SortByQuantity(salesOrderItem.getProduct()));
        return sites;
    }

    public void setOrderSuitableSites(SalesOrderItem salesOrderItem) throws IOException {
        setSuitableSites(salesOrderItem);
        deliveryAirSites = new ArrayList<ImportSite>();
        deliveryShipSites = new ArrayList<ImportSite>();
        //Check 1st priority: Delivery by ship
            
        for(ImportSite site: suitableSites) {
            if(checkFirstCondition(site) == true) deliveryAirSites.add(site);
            else deliveryShipSites.add(site);
        }

        //Check 2nd priority: sites have bigger quantity
        deliveryAirSites = sortSitesByQuantity(deliveryAirSites, salesOrderItem);
        deliveryShipSites = sortSitesByQuantity(deliveryShipSites, salesOrderItem);
        

    }

    public List<DeliveryOrder> getAllOrders() {
        return allOrders;
    }

    public void setAllOrders() throws IOException {
        allOrders = new ArrayList<DeliveryOrder>();

        childPanel.setLayout(new FlowLayout());
        remove(childPanel);

        contentBox = new JPanel();
        contentBox.setLayout(new GridLayout(salesOrderItems.size(), 1));
        // remove(contentBox);

        for(SalesOrderItem salesOrderItem: salesOrderItems) {
            orders = setOrders(salesOrderItem);
            orderBox = showData(orders, salesOrderItem);
            if(orders != null) allOrders.addAll(orders);
            contentBox.add(orderBox);
        }

        childPanel.add(backBtn);
        childPanel.add(submitBtn);
        add(contentBox, BorderLayout.CENTER);        
        add(childPanel, BorderLayout.PAGE_END);
    }

    public List<DeliveryOrder> setOrders(SalesOrderItem salesOrderItem) throws IOException {
        setOrderSuitableSites(salesOrderItem);
        int quantityOrdered = salesOrderItem.getQuantity();
        int quantity = 0;
        orders = new ArrayList<DeliveryOrder>();
        for(int i=0; i<deliveryShipSites.size(); i++) {
            if(quantity == quantityOrdered) break;
            ImportSite site = deliveryShipSites.get(i);
            for(SiteInstock instock: site.getInstocks()) {
                if(instock.getProduct().equals(salesOrderItem.getProduct())) {
                    if(instock.getQuantity() <= quantityOrdered - quantity) {
                        orders.add(new DeliveryOrder(site, salesOrderItem.getProduct(), instock.getQuantity(), salesOrderItem.getUnit(), DeliveryMethod.SHIP));
                        quantity += instock.getQuantity();
                    } else {
                        orders.add(new DeliveryOrder(site, salesOrderItem.getProduct(), (quantityOrdered - quantity), salesOrderItem.getUnit(), DeliveryMethod.SHIP));
                        quantity = quantityOrdered;
                    }
                }
            }
        }

        if(quantity < quantityOrdered) {
            for(int i=0; i<deliveryAirSites.size(); i++) {
                if(quantity == quantityOrdered) break;
                ImportSite site = deliveryAirSites.get(i);
                for(SiteInstock instock: site.getInstocks()) {
                    if(instock.getProduct().equals(salesOrderItem.getProduct())) {
                        if(instock.getQuantity() <= quantityOrdered - quantity) {
                            orders.add(new DeliveryOrder(site, salesOrderItem.getProduct(), instock.getQuantity(), salesOrderItem.getUnit(), DeliveryMethod.AIR));
                            quantity += instock.getQuantity();
                        } else {
                            orders.add(new DeliveryOrder(site, salesOrderItem.getProduct(), (quantityOrdered - quantity), salesOrderItem.getUnit(), DeliveryMethod.AIR));
                            quantity = quantityOrdered;
                        }
                    }
                }
            }
        }

        if(quantity < quantityOrdered) {
            return null;
        } else {
            for(DeliveryOrder order: orders) {
                ImportSite site = order.getSite();
                for(SiteInstock instock: site.getInstocks()) {
                    if(instock.getProduct().equals(salesOrderItem.getProduct())) {
                        instock.setQuantity(instock.getQuantity() - order.getQuantity());
                    }
                }
            }
            // allOrders.addAll(orders);
            return orders;
        }
    }

    private void setupButtons() {
        Icon checkIcon = IconFontSwing.buildIcon(FontAwesome.CHECK, 16);
        Icon backIcon = IconFontSwing.buildIcon(FontAwesome.TIMES, 16);
        submitBtn.setIcon(checkIcon);
        backBtn.setIcon(backIcon);
    }

    public ChooseSiteStep2Panel() {
        setLayout(new BorderLayout());
        setupButtons();
    }

    public JButton getBackBtn() {
        return backBtn;
    }

    public JButton getSubmitBtn() {
        return submitBtn;
    }

       
    public JPanel showData(List<DeliveryOrder> orders, SalesOrderItem salesOrderItem) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        JPanel orderBox = new JPanel();
        orderBox.setLayout(new BoxLayout(orderBox, BoxLayout.Y_AXIS));
		orderBox.add(new JLabel("Product: " + salesOrderItem.getProduct().getName() + " - Quantity: " + salesOrderItem.getQuantity() + " - Desired date: " + dateFormat.format(salesOrderItem.getDesiredDate())));
		
		if(orders == null) {
            JTextField text = new JTextField("Error: Cannot order this product!");
            orderBox.add(new JScrollPane(text), BorderLayout.CENTER);
        }
        else {
            String[] columnNames = {"SiteID", "ProductID", "Quantity", "Unit", "Delivery means"};
            String[][] values = new String[orders.size()][5];
        
            for(int i=0; i<orders.size(); i++) {
                DeliveryOrder order = orders.get(i);
                values[i][0] = Integer.toString(order.getSite().getId());
                values[i][1] = Integer.toString(order.getProduct().getId());
                values[i][2] = Integer.toString(order.getQuantity());
                values[i][3] = order.getUnit().toString();
                values[i][4] = order.getMethod().toString();
            }
            tableModel = new DefaultTableModel(values, columnNames);
            table = new JTable(tableModel);
            table.setRowSelectionAllowed(false);
        
            // table.setFont(new Font("Serif", Font.BOLD, 16));

            orderBox.add(new JScrollPane(table));
           
        }
        return orderBox;
    }

}
