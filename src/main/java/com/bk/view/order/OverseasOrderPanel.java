package com.bk.view.order;

import javax.swing.*;
import jiconfont.icons.font_awesome.FontAwesome;
import jiconfont.swing.IconFontSwing;
import java.awt.*;
import javax.swing.border.EmptyBorder;


public class OverseasOrderPanel extends JPanel{
    private JButton backBtn = new JButton("Back", IconFontSwing.buildIcon(FontAwesome.BACKWARD, 50));
    private JButton deliveryBtn = new JButton("Delivery orders", IconFontSwing.buildIcon(FontAwesome.PLANE, 50));
    private JButton instockBtn = new JButton("Site products", IconFontSwing.buildIcon(FontAwesome.INDUSTRY, 50));

    public JButton getInstockBtn() {
        return instockBtn;
    }

    public JButton getDeliveryBtn() {
        return deliveryBtn;
    }

    public OverseasOrderPanel() {
        JPanel childPanel = new JPanel();
        childPanel.setLayout(new FlowLayout());
        childPanel.setBorder(new EmptyBorder(100, 50, 50, 50));
        childPanel.add(backBtn);
        childPanel.add(deliveryBtn);
        childPanel.add(instockBtn);
        add(childPanel);
    }

    public JButton getBackBtn() {
        return backBtn;
    }

}
