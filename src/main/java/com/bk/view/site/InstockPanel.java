package com.bk.view.site;

import com.bk.App;
import com.bk.model.Product;
import com.bk.model.SalesOrder;
import com.bk.model.SalesOrderItem;

import jiconfont.icons.font_awesome.FontAwesome;
import jiconfont.swing.IconFontSwing;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.awt.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class InstockPanel extends JPanel {
    private JButton backBtn = new JButton("Back");
    //Add button for ask in-stock quantity (minhlh)
    private JButton instockBtn = new JButton("Ask in-stock quantity");
    //Add button for choose importing site (minhlh)
    private JButton chooseSiteBtn = new JButton("Choose Importing Site");

    private JScrollPane scrollPane = new JScrollPane();
    private JPanel childPanel = new JPanel();
    private JTable table;
    private DefaultTableModel tableModel;
    private List<SalesOrder> salesOrderList;

    public void setSalesOrderList(List<SalesOrder> salesOrderList) {
        this.salesOrderList = salesOrderList;
        showData();
    }

    private void setupButtons() {
        Icon plusIcon = IconFontSwing.buildIcon(FontAwesome.PLUS, 16);
        Icon backIcon = IconFontSwing.buildIcon(FontAwesome.BACKWARD, 16);
        instockBtn.setIcon(plusIcon);
        backBtn.setIcon(backIcon);
        chooseSiteBtn.setIcon(plusIcon);
    }

    public InstockPanel() {
        setLayout(new BorderLayout());
        setupButtons();
    }

    public JButton getBackBtn() {
        return backBtn;
    }
    
    public void showData() {
        childPanel.setLayout(new FlowLayout());
        
        remove(scrollPane);
        remove(childPanel);
        
        String[] columnNames = {"ID", "Username", "Role", "Product"};
        String[][] values = new String[salesOrderList.size()][4];
        for (int i = 0; i < salesOrderList.size(); i++) {
            SalesOrder order = salesOrderList.get(i);
            values[i][0] = Integer.toString(order.getId());
            values[i][1] = order.getUser().getName();
            values[i][2] = order.getUser().getRole().name();
            values[i][3] = "";
            Set<SalesOrderItem> items = order.getItems();
            Set<Product> orderProducts = new HashSet<Product>();
            for(SalesOrderItem item: items) {
                if(!orderProducts.contains(item.getProduct())) {
                    orderProducts.add(item.getProduct());
                }
            }
            for(Product orderProduct: orderProducts) {
                values[i][3] += orderProduct.getName() + ", ";
            }
            
        }
        tableModel = new DefaultTableModel(values, columnNames);
        table = new JTable(tableModel);
        table.setRowSelectionAllowed(true);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        // table.setFont(new Font("Serif", Font.BOLD, 16));

        scrollPane = new JScrollPane(table);

        add(scrollPane, BorderLayout.CENTER);
        childPanel.add(backBtn);
        childPanel.add(instockBtn);
        childPanel.add(chooseSiteBtn);
        
        add(childPanel, BorderLayout.PAGE_END);
    }

    public JButton getInstockBtn() {
        return instockBtn;
    }

    public JButton getChooseSiteBtn() {
        return chooseSiteBtn;
    }

    public int getSelectedRowIndex() {
        int i = table.getSelectedRow();
        if (i == -1) {
            return -1;
        }
        return i;
    }
}
