package com.bk.controller.sales;

import com.bk.App;
import com.bk.view.main.AbstractHomePanel;
import com.bk.view.sales.SalesPanel;

public class SalesController {
    private AbstractHomePanel homePanel;
    private SalesPanel salesPanel;

    public SalesController(AbstractHomePanel homePanel, SalesPanel salesPanel) {
        this.homePanel = homePanel;
        this.salesPanel = salesPanel;
        init();
    }

    private void init() {
        salesPanel.getBackBtn().addActionListener(actionEvent -> App.mainFrame.replacePanel(salesPanel, homePanel));
    }

}
