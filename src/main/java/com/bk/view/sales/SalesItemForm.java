package com.bk.view.sales;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.List;
import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.TitledBorder;

import com.bk.controller.sales.SalesItemController;
import com.bk.model.Product;
import com.bk.model.SalesOrderItem;
import com.bk.model.Unit;
import com.bk.utility.IProductService;
import com.bk.utility.ProductServiceImp;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import com.toedter.calendar.JDateChooser;

public class SalesItemForm extends JFrame {

	private JPanel contentPane;
	private JTextField tfSalesItemId;
	private JTextField tfQuantity;
	private JComboBox cbUnit;
	private JComboBox cbProduct = new JComboBox();
	private JDateChooser dateChooser = new JDateChooser();
	
	private JButton btnSave;
	private JButton btnDelete;
	
	private JLabel lblMessage;
	private JPanel panel;
	

	/**
	 * Create the frame.
	 */
	public SalesItemForm(SalesOrderItem salesItem, int orderId) {
		
		
		initComponent();
		
		SalesItemController controller = new SalesItemController(tfSalesItemId, tfQuantity, cbProduct, cbUnit, dateChooser,
																lblMessage, btnSave, btnDelete);
		controller.setView(salesItem);
		controller.setEvent(salesItem, orderId);
		
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				for (Window  window : Window.getWindows()) {
				    window.dispose();
				}
				JFrame mainSalesItemFrame = new MainSalesItemFrame(orderId);
				mainSalesItemFrame.setLocationRelativeTo(null);
				mainSalesItemFrame.setVisible(true);
			}
			
		});
	}
	
	
	public void initComponent() {
		setResizable(false);
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("../main/ecommerce-icon.png")));

		btnSave = new JButton("Save");
		
		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Sales Item Information", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		lblMessage = new JLabel("");
		
		JLabel lblSalesItemId = new JLabel("Sales Item ID:");
		JLabel lblMerchandiseCode = new JLabel("Merchandise:");
		JLabel lblQuantity = new JLabel("Quantity:");
		JLabel lblUnit = new JLabel("Unit:");
		JLabel lblDesiredDate = new JLabel("Desired Date:");

		
		tfSalesItemId = new JTextField();
		tfSalesItemId.setEditable(false);
		tfSalesItemId.setColumns(10);
		
		tfQuantity = new JTextField();
		tfQuantity.setColumns(10);
		cbUnit = new JComboBox(Unit.values());
		

		IProductService productService= new ProductServiceImp();
		List<Product> listProduct = productService.getList();
	    Vector<Product> productVector = new Vector<>(listProduct);
		cbProduct = new JComboBox<>(productVector);
		
		dateChooser = new JDateChooser();
		
		btnDelete = new JButton("Delete");
		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(18)
					.addComponent(lblMessage, GroupLayout.PREFERRED_SIZE, 247, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
					.addComponent(btnDelete, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnSave, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(1))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSave)
						.addComponent(lblMessage)
						.addComponent(btnDelete))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE))
		);
		
		
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblSalesItemId, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblQuantity, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblDesiredDate)
						.addComponent(lblMerchandiseCode, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(tfSalesItemId, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(dateChooser, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(tfQuantity, Alignment.LEADING))
							.addGap(107)
							.addComponent(lblUnit, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
							.addGap(27)
							.addComponent(cbUnit, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE))
						.addComponent(cbProduct, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(74, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(42)
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblMerchandiseCode)
							.addComponent(cbProduct, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblSalesItemId)
								.addComponent(tfSalesItemId, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(58)))
					.addGap(37)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblQuantity)
						.addComponent(tfQuantity, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(cbUnit, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblUnit))
					.addGap(47)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblDesiredDate)
						.addComponent(dateChooser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(55, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);

	}
}
