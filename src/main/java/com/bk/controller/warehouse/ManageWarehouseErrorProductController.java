package com.bk.controller.warehouse;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.bk.model.DeliveryOrder;
import com.bk.utility.IWarehouseService;
import com.bk.utility.SearchEvent;
import com.bk.utility.WarehouseTableModel;
import com.bk.utility.WarehouseServiceImp;
import com.bk.view.warehouse.WarehouseForm;

public class ManageWarehouseErrorProductController {

	private JPanel pnlView;
	private JTextField tfSearch;

	//	private JButton btnAdd;


	private IWarehouseService warehouseService = null;
	private final String[] listColumn = {"ID", "Merchandise Code", "Product Name", "Unit", "Quantity Order", "Quantity Received"};
	private TableRowSorter<TableModel>  rowSorter = null;

	public ManageWarehouseErrorProductController(JPanel pnlView, JTextField tfSearch) {
		super();
		this.pnlView = pnlView;
		this.tfSearch = tfSearch;
		this.warehouseService = new WarehouseServiceImp();
	}

	public void viewWarehouseErrorList() {
		List <DeliveryOrder> listItem = warehouseService.getWarehouseErrorList();

		DefaultTableModel model = new WarehouseTableModel().setTableProduct(listItem, listColumn);
		JTable table = new JTable(model);

		rowSorter = new TableRowSorter<TableModel>(table.getModel());
		table.setRowSorter(rowSorter);

		//add Search Event
		SearchEvent searchEvent = new SearchEvent();
		searchEvent.setSearchEvent(tfSearch, rowSorter);


		//add event table
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				if (e.getClickCount() == 2 && table.getSelectedRow() != -1) {
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					int selectedRowIndex = table.getSelectedRow();

					DeliveryOrder deliveryOrder = warehouseService.getWarehouseErrorList().get(selectedRowIndex);

					WarehouseForm form = new WarehouseForm(deliveryOrder);
					form.setLocationRelativeTo(null);
					form.setResizable(false);
					form.setTitle("Error Product Information");
					form.setVisible(true);
				}
			}
		});  
		
		// design
        table.getTableHeader().setFont(new Font("Arial", Font.BOLD, 14));
        table.getTableHeader().setPreferredSize(new Dimension(100, 50));
        table.setRowHeight(50);
        table.validate();
        table.repaint();
        
        JScrollPane scroll = new JScrollPane();
        scroll.getViewport().add(table);
        scroll.setPreferredSize(new Dimension(1350, 400));
        pnlView.removeAll();
        pnlView.setLayout(new CardLayout());
        pnlView.add(scroll);
        pnlView.validate();
        pnlView.repaint();
		
	}


}
