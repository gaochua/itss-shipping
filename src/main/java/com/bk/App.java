package com.bk;

import com.bk.controller.main.HomeController;
import com.bk.controller.main.LoginController;
import com.bk.controller.order.OverseasOrderController;
import com.bk.controller.sales.SalesController;
import com.bk.controller.site.SiteController;
import com.bk.model.User;
import com.bk.view.main.AbstractHomePanel;
import com.bk.view.main.HomePanel;
import com.bk.view.main.MainFrame;
import com.bk.view.order.OverseasOrderPanel;
import com.bk.view.sales.SalesPanel;
import com.bk.view.site.manageSite.SitePanel;
import com.formdev.flatlaf.FlatIntelliJLaf;
import jiconfont.icons.font_awesome.FontAwesome;
import jiconfont.swing.IconFontSwing;

import javax.swing.*;


public class App implements Runnable{
    public static MainFrame mainFrame;
    public static User currentUser;

    private static AbstractHomePanel homePanel;
    private static SitePanel sitePanel;

    public static AbstractHomePanel getHomePanel() {
		return homePanel;
	}

	public static SitePanel getSitePanel() {
		return sitePanel;
	}

	@Override
    public void run() {
        setupTheme();
        setupComponents();
        startUI();
    }

    private void startUI() {
        mainFrame.add(homePanel);
    }

    private void setupComponents() {
        homePanel = new HomePanel();
        OverseasOrderPanel orderPanel = new OverseasOrderPanel();
        SalesPanel salesPanel = new SalesPanel();
        sitePanel = new SitePanel();
        
        mainFrame = new MainFrame();
        new HomeController(homePanel, orderPanel, salesPanel, sitePanel);
        new OverseasOrderController(homePanel, orderPanel);
        new SalesController(homePanel, salesPanel);
        //new SiteController(homePanel, sitePanel);
        new LoginController();
    }

    private void setupTheme() {
        try {
            UIManager.setLookAndFeel( new FlatIntelliJLaf());
        } catch( Exception ex ) {
            System.err.println( "Failed to initialize LaF" );
        }
        IconFontSwing.register(FontAwesome.getIconFont());
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new App());
    }
}
