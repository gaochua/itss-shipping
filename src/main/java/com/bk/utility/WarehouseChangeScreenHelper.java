package com.bk.utility;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.bk.view.warehouse.WarehouseErrorProductPanel;
import com.bk.view.warehouse.WarehouseProcessingPanel;
import com.bk.view.warehouse.WarehouseValidProductPanel;


public class WarehouseChangeScreenHelper {
	private JPanel root;
	private String kindSelected = "";

	private List <ListBean> listItem = null;

	public WarehouseChangeScreenHelper(JPanel pnlRoot) {
		this.root = pnlRoot;

	}
	
	public void SetView(JPanel pnlItem, JLabel lblItem) {
		kindSelected = "ProcessingProduct";
		pnlItem.setBackground(new Color(96, 100, 191));
		lblItem.setBackground(new Color(96, 100, 191));

		root.removeAll();
		root.setLayout(new BorderLayout());
		root.add(new WarehouseProcessingPanel());
		root.validate();
		root.repaint();

	}
	
	public void setEvent(List<ListBean> listItem) {
		this.listItem = listItem;
		for (ListBean item : listItem) {
			item.getJpn().addMouseListener(new LabelEvent(item.getKind(), item.getJpn(), item.getJlb()));
		}
	}
	
	class LabelEvent implements MouseListener {

		private JPanel node;
		private String kind;

		private JPanel jpnItem;
		private JLabel jlbItem;

		public LabelEvent(String kind, JPanel jpnItem, JLabel jlbItem) {
			this.kind = kind;
			this.jpnItem = jpnItem;
			this.jlbItem = jlbItem;
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			switch (kind) {
			case "ProcessingProduct":
				node = new WarehouseProcessingPanel();
				break;
			case "WarehouseValidProduct":
				node = new WarehouseValidProductPanel();
				break;
			case "WarehouseErrorProduct":
				node = new WarehouseErrorProductPanel();
				break;
			default:
				break;
			}
			root.removeAll();
			root.setLayout(new BorderLayout());
			root.add(node);
			root.validate();
			root.repaint();
			setChangeBackground(kind);
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			kindSelected = kind;
			jpnItem.setBackground(new Color(96, 100, 191));
			jlbItem.setBackground(new Color(96, 100, 191));
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			jpnItem.setBackground(new Color(96, 100, 191));
			jlbItem.setBackground(new Color(96, 100, 191));
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			if (!kindSelected.equalsIgnoreCase(kind)) {
				jpnItem.setBackground(new Color(102,178,255));
				jlbItem.setBackground(new Color(102,178,255));
			}
			
		}
	}
	
	private void setChangeBackground(String kind) {
		for (ListBean item: listItem) {
			if (item.getKind().equalsIgnoreCase(kind)) {
				item.getJpn().setBackground(new Color (96,100,191));
				item.getJlb().setBackground(new Color (96,100,191));
			} else {
				item.getJpn().setBackground(new Color (102,178,255));
				item.getJlb().setBackground(new Color (102,178,255));
			}

		}

	}


}
