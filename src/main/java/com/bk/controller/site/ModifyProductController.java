package com.bk.controller.site;

import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.bk.App;
import com.bk.model.ImportSite;
import com.bk.model.Product;
import com.bk.model.SiteInstock;
import com.bk.model.Unit;
import com.bk.utility.HibernateUtil;
import com.bk.utility.ProductTableModel;
import com.bk.view.site.manageSite.ModifyProductForm;
import com.bk.view.site.manageSite.ModifyProductFrame;

public class ModifyProductController extends SiteController {
	
	private static SiteInstock instock;
	
	public static SiteInstock getInstock() {
		return instock;
	}

	public ModifyProductController() {
		table.addMouseListener(new MouseAdapter() {
        	public void mouseClicked(MouseEvent e) {
        		if (e.getClickCount() == 2) {
        			int row = table.getSelectedRow();
        			int column = table.getSelectedColumn();
        			if (column == 5) {
        				table.setValueAt(! (Boolean) table.getValueAt(row, column), row, column);
        			}
        			else {
						instock = hibernateGetSiteInstock((String) table.getValueAt(row, ProductTableModel.PRODUCT_COLUMN), site.getName());
						modifyProductFrame = new ModifyProductFrame(instock);
        				initSiteModifyProductFrame();
        				modifyProductFrame.pack();
        				modifyProductFrame.setLocationRelativeTo(App.mainFrame);
        				modifyProductFrame.setVisible(true);
        			}
        		}
        	};
        });
	}
	
	private void initSiteModifyProductFrame() {
		modifyProductFrame.getCancelBtn().addActionListener(ae -> {
			modifyProductFrame.dispose();
		});
		
		ModifyProductForm form = modifyProductFrame.getForm();
		JComboBox<String> products = form.get(1);
		JTextField productNameField = form.get(3);
		JTextArea descriptionArea = form.get(5);
		products.addActionListener(ae -> {
			if (products.getSelectedIndex() > 0) {
				productNameField.setEditable(false);
				productNameField.setText(form.getContent(1));
				descriptionArea.setEditable(false);
				String description = "";
				try {
					description = hibernateGetProduct(form.getContent(1), "", 0).getDescription();
				} catch (InvalidDataException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				descriptionArea.setText(description);
			}
			else {
				productNameField.setEditable(true);
				productNameField.setText("");
				descriptionArea.setEditable(true);
				descriptionArea.setText("");
			}
			form.updateUI();
		});
		products.getActionListeners()[0].actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
		
		modifyProductFrame.getSubmitBtn().addActionListener(ae -> {
			String productName = form.getContent(3);
			if (productName.isEmpty()) {
				JOptionPane.showMessageDialog(null, "Product name can't be empty!", "ERROR", JOptionPane.ERROR_MESSAGE);
				return;
			}
			String description = form.getContent(5);
			Unit unit = form.getContent(7);
			
			Integer quantity = 0;
			try {
				quantity = form.getContent(9);
			}
			catch (Exception e) {
				JOptionPane.showMessageDialog(null, "quantity can't be empty!", "ERROR", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			int choice = JOptionPane.showConfirmDialog(null, "Confirm modifying this product? " + productName, "UPDATE", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
			if (choice == JOptionPane.YES_OPTION) {
				try {
					modifyProduct(productName, description, unit, quantity, site);
				} catch (InvalidDataException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				table.setModel(new ProductTableModel(site));
				JOptionPane.showMessageDialog(null, "Modify product successfully!", "SUCCESS", JOptionPane.INFORMATION_MESSAGE);
			}
			modifyProductFrame.dispose();
		});
	}
	
	public void modifyProduct(String name, String description, Unit unit, Integer quantity, ImportSite site) throws InvalidDataException {
		Product product = hibernateGetProduct(name, description, 1);
		instock.setProduct(product);
		instock.setQuantity(quantity);
		instock.setUnit(unit);
		instock.setSite(site);
		HibernateUtil.process(session -> session.update(instock));
	}

}
