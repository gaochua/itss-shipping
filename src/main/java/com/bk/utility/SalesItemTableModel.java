package com.bk.utility;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import com.bk.model.SalesOrderItem;

public class SalesItemTableModel {
	
	public DefaultTableModel setTableSalesItem(List <SalesOrderItem> listItem, String[] listColumn) {
		DefaultTableModel dtm = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int rowIndex, int colIndex) {
				return false;
			}
		};
		dtm.setColumnIdentifiers(listColumn);
		Object obj[] = null;
		
		SalesOrderItem salesOrderItem = null;
		int columns = listColumn.length;
		int rows = listItem.size();
		if (rows > 0) {
			for (int i = 0; i < rows; i++) {
				salesOrderItem = listItem.get(i);
	            obj = new Object[columns];
				obj[0] = (salesOrderItem.getId());
				obj[1] = salesOrderItem.getProduct().getId();
				obj[2] = salesOrderItem.getProduct().getName();
				obj[3] = (salesOrderItem.getQuantity());
				obj[4] = salesOrderItem.getUnit();
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//				Date dateWithoutTime = sdf.parse(sdf.format(new Date()));
				obj[5] = sdf.format(salesOrderItem.getDesiredDate());
//				obj[6] = salesOrderItem.getOrder().getId();

	            dtm.addRow(obj);
			}
		}
		return dtm;
	}

}
