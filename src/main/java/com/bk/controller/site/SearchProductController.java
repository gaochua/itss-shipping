package com.bk.controller.site;

import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class SearchProductController extends SiteController {
	
	public SearchProductController() {
		JTextField searchField = sitePanel.getSearchField();
		
        searchField.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				doTheThing();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				doTheThing();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				doTheThing();
			}
			
			@SuppressWarnings("unchecked")
			public void doTheThing() {
				String text = sitePanel.getSearchField().getText();
				if (text.trim().length() == 0) {
					((TableRowSorter<TableModel>) table.getRowSorter()).setRowFilter(null);
				}
				else {
					((TableRowSorter<TableModel>) table.getRowSorter()).setRowFilter(RowFilter.regexFilter("(?i)" + text));
				}
			}
		});

	}

}
