package com.bk.controller;

import java.util.Comparator;
import java.util.Set;

import com.bk.model.ImportSite;
import com.bk.model.Product;
import com.bk.model.SiteInstock;

public class SortByQuantity implements Comparator<ImportSite> {
    private Product product;

    public SortByQuantity(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    @Override
    public int compare(ImportSite o1, ImportSite o2) {
        // TODO Auto-generated method stub
        int quantity1 = 0;
        int quantity2 = 0;
        Set<SiteInstock> instocks1 = o1.getInstocks();
        Set<SiteInstock> instocks2 = o2.getInstocks();
        for(SiteInstock instock: instocks1) {
            if(instock.getProduct().equals(product)) {
                quantity1 = instock.getQuantity();
            }
        }
        for(SiteInstock instock: instocks2) {
            if(instock.getProduct().equals(product)) {
                quantity2 = instock.getQuantity();
            }
        }
        return quantity1 - quantity2;
    }

}