package com.bk.utility;

import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.text.DocumentFilter.FilterBypass;
import javax.swing.text.Element;
import javax.swing.text.PlainDocument;

public class IntegerFilterDocument extends PlainDocument {
	
	public static final int lineLimit = 5;

	public IntegerFilterDocument() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	@Override
	public void replace(int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
		// TODO Auto-generated method stub
        if (getLength() + text.length() - length <= lineLimit && text.matches("^[0-9]+[.]?[0-9]{0,1}$")) {
            super.replace(offset,  length, text, attrs);
        }
	}
	
	@Override
	public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
		// TODO Auto-generated method stub
		if (getLength() + str.length() <= lineLimit && str.matches("^[0-9]+[.]?[0-9]{0,1}$")) {
			super.insertString(offs, str, a);
		}
	}
	
}
