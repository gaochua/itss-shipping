package com.bk.view.site.manageSite;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.bk.model.SiteInstock;

@SuppressWarnings("serial")
public class ModifyProductFrame extends JFrame {

	private JButton submitBtn = new JButton("Submit", SitePanel.updateIcon);
	private JButton cancelBtn = new JButton("Cancel", SitePanel.cancelIcon);
	private ModifyProductForm form;
		
	public ModifyProductFrame(SiteInstock instock) {
		setTitle("Modify Product Information");
		setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		setMinimumSize(new Dimension(200, 300));
		
		form = new ModifyProductForm(instock);
		JPanel formPanel = new JPanel();
		formPanel.add(form);
		formPanel.setAlignmentX(CENTER_ALIGNMENT);
		add(formPanel);
		
		JPanel btnsPanel = new JPanel();
		btnsPanel.add(submitBtn);
		btnsPanel.add(cancelBtn);
		add(btnsPanel);
	}

	public JButton getSubmitBtn() {
		return submitBtn;
	}

	public JButton getCancelBtn() {
		return cancelBtn;
	}

	public ModifyProductForm getForm() {
		return form;
	}

}
