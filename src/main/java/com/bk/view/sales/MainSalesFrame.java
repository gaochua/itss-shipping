package com.bk.view.sales;

import com.bk.App;

import jiconfont.icons.font_awesome.FontAwesome;
import jiconfont.swing.IconFontSwing;


import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;


import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.Icon;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;

public class MainSalesFrame extends JFrame {

	private JPanel contentPane;
	private JPanel pnlMenuSales;
	private JPanel pnlMainScreen;
	
	private JButton btnBack; 

	
	/**
	 * Create the frame.
	 */
	public MainSalesFrame() {
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("BK Shipping");
		
		// full size
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screenSize.width, screenSize.height);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("../main/ecommerce-icon.png")));

//		setBounds(100, 100, 600, 350);
		setLocationRelativeTo(null);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		pnlMenuSales = new JPanel();
		pnlMenuSales.setBackground(new Color(204,229,255));

		pnlMainScreen = new JPanel();
		
		JPanel pnlHeadSalesOrder = new JPanel();
		pnlHeadSalesOrder.setBackground(Color.BLUE);

		btnBack = new JButton("Log out");
		btnBack.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(btnBack);
				frame.setVisible(false);
				frame.dispose();
				App.mainFrame.setVisible(true);
			}
		});

		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(pnlMenuSales, GroupLayout.PREFERRED_SIZE, 207, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(pnlMainScreen, GroupLayout.DEFAULT_SIZE, 364, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(pnlMenuSales, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(pnlMainScreen, GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE))
					.addGap(0))
		);

		
		GroupLayout gl_pnlMenuSales = new GroupLayout(pnlMenuSales);
		gl_pnlMenuSales.setHorizontalGroup(
			gl_pnlMenuSales.createParallelGroup(Alignment.LEADING)
				.addComponent(pnlHeadSalesOrder, GroupLayout.PREFERRED_SIZE, 194, Short.MAX_VALUE)
				.addGroup(gl_pnlMenuSales.createSequentialGroup()
					.addContainerGap(55, Short.MAX_VALUE)
					.addComponent(btnBack, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
					.addGap(44))
		);
		gl_pnlMenuSales.setVerticalGroup(
			gl_pnlMenuSales.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlMenuSales.createSequentialGroup()
					.addComponent(pnlHeadSalesOrder, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 190, Short.MAX_VALUE)
					.addComponent(btnBack, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);

		JLabel lblHeadSalesOrder = new JLabel("Manage Sale Order");
		lblHeadSalesOrder.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblHeadSalesOrder.setForeground(Color.BLACK);
		Icon icon = IconFontSwing.buildIcon(FontAwesome.SHOPPING_CART, 50);
		lblHeadSalesOrder.setIcon(icon);
		
		GroupLayout gl_pnlHeadSalesOrder = new GroupLayout(pnlHeadSalesOrder);
		gl_pnlHeadSalesOrder.setHorizontalGroup(
				gl_pnlHeadSalesOrder.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_pnlHeadSalesOrder.createSequentialGroup()
						.addContainerGap()
						.addComponent(lblHeadSalesOrder, GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
						.addContainerGap())
				);
		gl_pnlHeadSalesOrder.setVerticalGroup(
				gl_pnlHeadSalesOrder.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlHeadSalesOrder.createSequentialGroup()
						.addContainerGap()
						.addComponent(lblHeadSalesOrder, GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
						.addContainerGap())
				);
		pnlHeadSalesOrder.setLayout(gl_pnlHeadSalesOrder);
		pnlMenuSales.setLayout(gl_pnlMenuSales);
		contentPane.setLayout(gl_contentPane);
		
		pnlMainScreen.removeAll();
		pnlMainScreen.setLayout(new BorderLayout());
		pnlMainScreen.add(new SalesOrderPanel());
		pnlMainScreen.validate();
		pnlMainScreen.repaint();
		
	}

}
