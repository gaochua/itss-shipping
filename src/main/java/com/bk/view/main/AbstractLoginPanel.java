package com.bk.view.main;

import javax.swing.*;

public abstract class AbstractLoginPanel extends JPanel{
    protected JTextField usernameField;
    protected JPasswordField passwordField;
    protected JButton submitBtn;
    protected JButton backBtn;

    public JButton getSubmitBtn() {
        return submitBtn;
    }

    public JButton getBackBtn() {
        return backBtn;
    }

    public JTextField getUsernameField() {
        return usernameField;
    }

    public JPasswordField getPasswordField() {
        return passwordField;
    }

}
