package com.bk.model;

public enum DeliveryMethod {
    AIR, SHIP
}
