# BK shipping

## Build

``` bash
mvn clean compile assembly:single
java -jar target/shipping-1.0-SNAPSHOT-jar-with-dependencies.jar 
```

## Notes
1. dùng `maven` để build và chạy -> dùng IDE gì cũng dc 
2. các bảng sql là class trong package `com.bk.model`
3. tách biệt view (hiển thị) và controller (xử lý logic)
4. 1 `frame` duy nhất
5. nhiều `panel`
6. dùng class `HibernateUtil` để crud với database
7. app chạy dc đã, xấu đẹp tính sau 
8. dùng `intellij` có thể tạo diagram (sequence/class ...) từ code (chưa thử các ide khác)
