package com.bk.view.site.manageSite;

import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.bk.model.Unit;
import com.bk.utility.IntegerFilterDocument;

@SuppressWarnings("serial")
public class AddProductForm extends FillableForm {
	
	public AddProductForm() {
		super();
	}

	@Override
	public void layoutContent() {
		// TODO Auto-generated method stub
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JLabel newProductNameLabel = new JLabel("New product's name:");
		JTextField newProductName = new JTextField();
		JLabel productDescriptionLabel = new JLabel("New product's description:");
		JTextArea description = new JTextArea();
		JLabel productUnitLabel = new JLabel("Unit:");
		JComboBox<Unit> units = new JComboBox<Unit>(Unit.values());
		JLabel productQuantityLabel = new JLabel("Quantity:");
		JTextField quantity = new JTextField();
		quantity.setDocument(new IntegerFilterDocument());
		
		add(newProductNameLabel);
		add(newProductName);
		add(productDescriptionLabel);
		add(description);
		add(productUnitLabel);
		add(units);
		add(productQuantityLabel);
		add(quantity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <E> E getContent(int index) {
		// TODO Auto-generated method stub
		if (index % 2 == 0) {
			return (E) ((JLabel) get(index)).getText();
		}
		else {
			if (index == 1) {
				return (E) ((JTextField) get(index)).getText();
			}
			else if (index == 3) {
				return (E) ((JTextArea) get(index)).getText();
			}
			else if (index == 5) {
				return (E) ((JComboBox<Unit>) get(index)).getSelectedItem();
			}
			else {
				return (E) ((Integer) Integer.parseInt(((JTextField) get(index)).getText()));
			}
		}
	}

}
