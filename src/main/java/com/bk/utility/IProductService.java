package com.bk.utility;

import java.util.List;

import com.bk.model.Product;

public interface IProductService {
	
	public List<Product> getList();
	
	public Product getById(int productId);


}
