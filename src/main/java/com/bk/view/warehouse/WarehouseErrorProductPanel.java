package com.bk.view.warehouse;

import java.awt.Color;

import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;

import com.bk.controller.warehouse.ManageWarehouseErrorProductController;
import com.bk.controller.warehouse.ManageWarehouseValidProductController;

public class WarehouseErrorProductPanel extends JPanel {
	private JTextField tfSearch;

	private JPanel pnlView; 


	/**
	 * Create the panel.
	 */
	public WarehouseErrorProductPanel() {
		initComponent();

		ManageWarehouseErrorProductController controller = new ManageWarehouseErrorProductController(pnlView, tfSearch);
		controller.viewWarehouseErrorList();

	}

	public void initComponent() {

		tfSearch = new JTextField();
		tfSearch.setColumns(10);

		pnlView = new JPanel();
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(pnlView, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)
								.addComponent(tfSearch, GroupLayout.PREFERRED_SIZE, 213, GroupLayout.PREFERRED_SIZE))
						.addContainerGap())
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addComponent(tfSearch, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(18)
						.addComponent(pnlView, GroupLayout.DEFAULT_SIZE, 237, Short.MAX_VALUE)
						.addContainerGap())
				);
		setLayout(groupLayout);

	}

}
