package com.bk.controller.main;

import com.bk.model.Role;
import com.bk.model.User;
import com.bk.view.main.AbstractLoginPanel;

import javax.swing.*;

public abstract class AbstractLoginController extends AbstractController{
    protected AbstractLoginPanel loginPanel;
    protected Role loginRole;
    protected JPanel prevPanel;
    protected JPanel nextPanel;

    public abstract User findUser(String username, String password, Role userRole);

    public void setPrevPanel(JPanel prevPanel) {
        this.prevPanel = prevPanel;
    }

    public void setNextPanel(JPanel nextPanel) {
        this.nextPanel = nextPanel;
    }

    public void setLoginRole(Role loginRole) {
        this.loginRole = loginRole;
    }

    public AbstractLoginPanel getLoginPanel() {
        return loginPanel;
    }

//    public AbstractLoginController() {
//        setupPanels();
//    }
}
