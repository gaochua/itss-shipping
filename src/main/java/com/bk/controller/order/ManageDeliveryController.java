package com.bk.controller.order;

import com.bk.App;
import com.bk.utility.HibernateUtil;
import com.bk.utility.UIHelper;
import com.bk.model.*;
import com.bk.view.order.CreateDeliveryPanel;
import com.bk.view.order.OverseasOrderPanel;
import com.bk.view.order.UpdateDeliveryPanel;
import com.bk.view.order.ViewDeliveryPanel;

import java.util.List;

public class ManageDeliveryController {
    private OverseasOrderPanel orderPanel;
    private CreateDeliveryPanel createDeliveryPanel = new CreateDeliveryPanel();
    private ViewDeliveryPanel viewDeliveryPanel = new ViewDeliveryPanel();
    private UpdateDeliveryPanel updateDeliveryPanel = new UpdateDeliveryPanel();

    List<DeliveryOrder> orders;
    List<Product> products;
    List<ImportSite> sites;

    public ManageDeliveryController(OverseasOrderPanel orderPanel) {
        this.orderPanel = orderPanel;
    }

    public void init(List<DeliveryOrder> orders, List<Product> products, List<ImportSite> sites) {
        this.orders = orders;
        this.products = products;
        this.sites = sites;

        initViewPanel();
        initCreatePanel();
        initUpdatePanel();

        App.mainFrame.replacePanel(orderPanel, viewDeliveryPanel);
    }

    private void initUpdatePanel() {
        updateDeliveryPanel.getSubmitBtn().addActionListener(ae -> {
            try {
                getOrderFromPanel(updateDeliveryPanel, updateDeliveryPanel.getDeliveryOrder());
            } catch (RuntimeException re) {
                UIHelper.showErrorDialog(re.getMessage());
                return;
            }
            viewDeliveryPanel.setDeliveryOrderList(orders);
            UIHelper.showSuccessDialog("Update order successfully!");
            App.mainFrame.replacePanel(updateDeliveryPanel, viewDeliveryPanel);
            HibernateUtil.process(session -> session.update(updateDeliveryPanel.getDeliveryOrder()));
        });
        updateDeliveryPanel.getBackBtn().addActionListener(ae -> {
            viewDeliveryPanel.setDeliveryOrderList(orders);
            App.mainFrame.replacePanel(updateDeliveryPanel, viewDeliveryPanel);
        });
    }

    private void initCreatePanel() {
        createDeliveryPanel.getBackBtn().addActionListener(ae -> App.mainFrame.replacePanel(createDeliveryPanel, viewDeliveryPanel));
        createDeliveryPanel.getSubmitBtn().addActionListener(ae -> {
            DeliveryOrder newOrder;
            try {
                newOrder = new DeliveryOrder();
                getOrderFromPanel(createDeliveryPanel, newOrder);
            } catch (RuntimeException re) {
                UIHelper.showErrorDialog(re.getMessage());
                return;
            }
            HibernateUtil.process(session -> session.save(newOrder));
            UIHelper.showSuccessDialog("Create order successfully!");
            orders.add(newOrder);
            viewDeliveryPanel.setDeliveryOrderList(orders);
            App.mainFrame.replacePanel(createDeliveryPanel, viewDeliveryPanel);
        });
    }

    private void initViewPanel() {
        viewDeliveryPanel.setDeliveryOrderList(orders);
        viewDeliveryPanel.getCreateBtn().addActionListener(actionEvent -> {
            createDeliveryPanel.updateDisplay(null, products, sites);
            App.mainFrame.replacePanel(viewDeliveryPanel, createDeliveryPanel);
        });
        viewDeliveryPanel.getUpdateBtn().addActionListener(actionEvent -> {
            int index = viewDeliveryPanel.getSelectedRowIndex();
            updateDeliveryPanel.updateDisplay(orders.get(index), products, sites);
            App.mainFrame.replacePanel(viewDeliveryPanel, updateDeliveryPanel);
        });
        viewDeliveryPanel.getDeleteBtn().addActionListener(actionEvent -> {
            int index = viewDeliveryPanel.getSelectedRowIndex();
            HibernateUtil.process(session -> session.delete(orders.get(index)));
            UIHelper.showSuccessDialog("Delete order successfully!");
            orders.remove(index);
            viewDeliveryPanel.setDeliveryOrderList(orders);
        });
        viewDeliveryPanel.getBackBtn().addActionListener(actionEvent -> App.mainFrame.replacePanel(viewDeliveryPanel, orderPanel));
    }

    private void getOrderFromPanel(UpdateDeliveryPanel panel, DeliveryOrder order) {
        Unit unit = (Unit) panel.getUnitBox().getSelectedItem();
        Product product = (Product) panel.getProductBox().getSelectedItem();
        ImportSite importSite = (ImportSite) panel.getSiteBox().getSelectedItem();
        DeliveryMethod method = (DeliveryMethod) panel.getMethodBox().getSelectedItem();
        int quantity;
        try {
            quantity = Integer.parseInt(panel.getQuantityField().getText());
        } catch (NumberFormatException nfe) {
            throw new RuntimeException("invalid quantity!");
        }
        order.setUnit(unit);
        order.setProduct(product);
        order.setSite(importSite);
        order.setMethod(method);
        order.setQuantity(quantity);
    }
}
