package com.bk.controller.main;

import com.bk.App;
import com.bk.controller.site.SiteController;
import com.bk.utility.HibernateUtil;
import com.bk.utility.UIHelper;
import com.bk.model.Role;
import com.bk.model.User;
import com.bk.view.main.LoginPanel;
import com.bk.view.sales.MainSalesFrame;
import com.bk.view.warehouse.MainWarehouseFrame;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

public class LoginController extends AbstractLoginController {
    @Override
    public User findUser(String username, String password, Role userRole) {
        String hql = "From User U Where U.name = :username and U.password = :password";
        List<User> results = HibernateUtil.get(session -> session.createQuery(hql)
                .setParameter("username", username).setParameter("password", password)
                .list());
        return results.size() == 0 ? null : results.get(0);
    }

    @Override
    public void setupPanels() {
        loginPanel = new LoginPanel();
        loginPanel.getSubmitBtn().addActionListener(ae -> {
            String username = loginPanel.getUsernameField().getText();
            String password = loginPanel.getPasswordField().getText();
            User foundUser = findUser(username, password, loginRole);
            if (foundUser == null) {
                UIHelper.showErrorDialog("Login failed!");
            } else if (foundUser.getRole() == Role.SALES_EMPLOYEE) {
                new MainSalesFrame().setVisible(true);
                loginPanel.getUsernameField().setText("");
                loginPanel.getPasswordField().setText("");
                UIHelper.showSuccessDialog("Login successfully!");
                App.mainFrame.setVisible(false);
            } else if (foundUser.getRole() == Role.WAREHOUSE_EMPLOYEE) {
                new MainWarehouseFrame().setVisible(true);
                loginPanel.getUsernameField().setText("");
                loginPanel.getPasswordField().setText("");
                UIHelper.showSuccessDialog("Login successfully!");
                App.mainFrame.setVisible(false);
            } else {
                App.currentUser = foundUser;
                new SiteController(App.getHomePanel() , App.getSitePanel(), foundUser.getSite());
                App.mainFrame.replacePanel(loginPanel, nextPanel);
                loginPanel.getUsernameField().setText("");
                loginPanel.getPasswordField().setText("");
                UIHelper.showSuccessDialog("Login successfully!");
            }
        });
        KeyAdapter keyAdapter = new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        loginPanel.getSubmitBtn().doClick();
                    }
                }
            };
        loginPanel.getUsernameField().addKeyListener(keyAdapter);
        loginPanel.getPasswordField().addKeyListener(keyAdapter);
        loginPanel.getBackBtn().addActionListener(ae -> App.mainFrame.replacePanel(loginPanel, prevPanel));
    }
}
