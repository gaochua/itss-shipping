package com.bk.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "sales_order")
public class SalesOrder implements DataModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name="user_id", nullable=false)
    private User user;

    @OneToMany(mappedBy="order", fetch = FetchType.EAGER)
    private Set<SalesOrderItem> items = new HashSet<>();

    public SalesOrder(User user) {
        this.user = user;
    }

    public SalesOrder() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<SalesOrderItem> getItems() {
        return items;
    }

    public void setItems(Set<SalesOrderItem> items) {
        this.items = items;
    }
}
