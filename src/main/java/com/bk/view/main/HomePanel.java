package com.bk.view.main;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import com.bk.view.main.AbstractHomePanel;
import jiconfont.icons.font_awesome.FontAwesome;
import jiconfont.swing.IconFontSwing;

import java.awt.*;

public class HomePanel extends AbstractHomePanel {
    public HomePanel() {
        JPanel childPanel = new JPanel();

        childPanel.setLayout(new FlowLayout());
        childPanel.setBorder(new EmptyBorder(100, 50, 50, 50));
        siteBtn = new JButton("Site employee", IconFontSwing.buildIcon(FontAwesome.WEIXIN, 50));
        salesBtn = new JButton("Sales employee", IconFontSwing.buildIcon(FontAwesome.SHOPPING_CART, 50));
        orderBtn = new JButton("Overseas order employee", IconFontSwing.buildIcon(FontAwesome.GLOBE, 50));
        warehouseBtn = new JButton("Warehouse employee", IconFontSwing.buildIcon(FontAwesome.HOME, 50));
        childPanel.add(new JLabel("Welcome visitor!"));
        childPanel.add(siteBtn);
        childPanel.add(salesBtn);
        childPanel.add(orderBtn);
        childPanel.add(warehouseBtn);
        add(childPanel, BorderLayout.CENTER);
    }
}
