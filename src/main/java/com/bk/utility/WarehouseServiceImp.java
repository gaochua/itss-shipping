package com.bk.utility;

import java.util.ArrayList;
import java.util.List;

import com.bk.model.DeliveryOrder;

public class WarehouseServiceImp implements IWarehouseService{

	@Override
	public List<DeliveryOrder> getProcessingList() {
		// TODO Auto-generated method stub
		List<DeliveryOrder> list = new ArrayList<>();
		list = HibernateUtil.get(session -> session.createQuery("From DeliveryOrder Where isChecked =: isChecked").setParameter("isChecked", false).list());
		return list;
	}

	@Override
	public void update(DeliveryOrder deliveryOrder) {
		// TODO Auto-generated method stub
		HibernateUtil.process(session -> session.update(deliveryOrder));

	}

	@Override
	public List<DeliveryOrder> getWarehouseProductList() {
		// TODO Auto-generated method stub
		List<DeliveryOrder> list = new ArrayList<>();
		list = HibernateUtil.get(session -> session.createQuery("From DeliveryOrder Where isChecked =: isChecked And quantity = quantityReceived").setParameter("isChecked", true).list());

		return list;

	}

	@Override
	public List<DeliveryOrder> getWarehouseErrorList() {
		// TODO Auto-generated method stub
		List<DeliveryOrder> list = new ArrayList<>();
		list = HibernateUtil.get(session -> session.createQuery("From DeliveryOrder Where isChecked =: isChecked And quantity != quantityReceived").setParameter("isChecked", true).list());

		return list;
	}
	

}
