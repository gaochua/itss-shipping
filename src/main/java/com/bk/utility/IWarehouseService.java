package com.bk.utility;

import java.util.List;

import com.bk.model.DeliveryOrder;

public interface IWarehouseService {
	
	public List<DeliveryOrder> getProcessingList();
	
	public List<DeliveryOrder> getWarehouseProductList();
	
	public List<DeliveryOrder> getWarehouseErrorList();
	
	public void update(DeliveryOrder deliveryOrder);
	
	

}
