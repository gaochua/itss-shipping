package com.bk.view.site;

import com.bk.App;

import com.bk.model.SalesOrder;
import com.bk.model.SalesOrderItem;

import jiconfont.icons.font_awesome.FontAwesome;
import jiconfont.swing.IconFontSwing;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ChooseSiteStep1Panel extends JPanel {
    private JButton backBtn = new JButton("Cancel");
    private JButton nextBtn = new JButton("Next");
    
    private JScrollPane scrollPane = new JScrollPane();
    private JPanel childPanel = new JPanel();
    private JTable table;
    private DefaultTableModel tableModel;
    private SalesOrder salesOrder;
    private List<SalesOrderItem> salesOrderItems;

    public void setSalesOrder(SalesOrder salesOrder) {
        this.salesOrder = salesOrder;
    }

    public List<SalesOrderItem> getSalesOrderItems() {
        return salesOrderItems;
    }

    public void setSalesOrderItems(List<SalesOrderItem> salesOrderItems) {
        this.salesOrderItems = salesOrderItems;
    }

    public void setSalesOrderItems() {
        salesOrderItems = new ArrayList<SalesOrderItem>();
        for(SalesOrderItem item: salesOrder.getItems()) {
            salesOrderItems.add(item);
           
        }
        showData();
    }

    private void setupButtons() {
        Icon nextIcon = IconFontSwing.buildIcon(FontAwesome.FORWARD, 16);
        Icon backIcon = IconFontSwing.buildIcon(FontAwesome.BACKWARD, 16);
        nextBtn.setIcon(nextIcon);
        backBtn.setIcon(backIcon);
    }

    public ChooseSiteStep1Panel() {
        setLayout(new BorderLayout());
        setupButtons();
    }

    public JButton getBackBtn() {
        return backBtn;
    }

    public JButton getNextBtn() {
        return nextBtn;
    }
    
    public void showData() {
        childPanel.setLayout(new FlowLayout());
        
        remove(scrollPane);
        remove(childPanel);

        if(salesOrderItems == null) {
            JTextField text = new JTextField("All products are ordered");
            scrollPane = new JScrollPane(text);
            add(scrollPane, BorderLayout.CENTER);
            childPanel.add(backBtn);
            add(childPanel, BorderLayout.PAGE_END);
        }
        else {
            String[] columnNames = {"ID", "Name", "Quantity", "Unit", "Desired delivery date"};
            String[][] values = new String[salesOrderItems.size()][5];
        
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            for(int i=0; i<salesOrderItems.size(); i++) {
                SalesOrderItem item = salesOrderItems.get(i);
                values[i][0] = Integer.toString(item.getProduct().getId());
                values[i][1] = item.getProduct().getName();
                values[i][2] = Integer.toString(item.getQuantity());
                values[i][3] = item.getUnit().toString();
                values[i][4] = dateFormat.format(item.getDesiredDate());
            
            }
            tableModel = new DefaultTableModel(values, columnNames);
            table = new JTable(tableModel);
            table.setRowSelectionAllowed(false);
            //table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            // table.setFont(new Font("Serif", Font.BOLD, 16));

            scrollPane = new JScrollPane(table);

            add(scrollPane, BorderLayout.CENTER);
            childPanel.add(backBtn);
            childPanel.add(nextBtn);
                
            add(childPanel, BorderLayout.PAGE_END);
        }

        
    }

}
