package com.bk.utility;

import java.util.ArrayList;
import java.util.List;

import com.bk.model.SalesOrder;
import com.bk.model.User;

public class SalesOrderServiceImp implements ISalesOrderService{

	@Override
	public List<SalesOrder> getList() {
		// TODO Auto-generated method stub
		List<SalesOrder> list = new ArrayList<>();
		list = HibernateUtil.get(session -> session.createQuery("From SalesOrder").list());
		return list;
	}

	@Override
	public int createSalesOrder(SalesOrder salesOrder) {
		// TODO Auto-generated method stub
		List<User> userList = new ArrayList<>();
		userList = HibernateUtil.get(session -> session.createQuery("From User Where id = 1").list());
//		User sales_user = new User("sales", "sales", Role.SALES_EMPLOYEE);
		
		User sales_user = userList.get(0);
		salesOrder.setUser(sales_user);
		HibernateUtil.process(session -> session.save(salesOrder));
		return salesOrder.getId();
//		return 0;
	}

	@Override
	public SalesOrder getById(int orderId) {
		// TODO Auto-generated method stub
		List<SalesOrder> list = new ArrayList<>();
		list = HibernateUtil.get(session -> session.createQuery("From SalesOrder Where id =: orderId").setParameter("orderId", orderId).list());
	
		return list.get(0);
	}

	@Override
	public void deleteSalesOrder(SalesOrder saleOrder) {
		// TODO Auto-generated method stub
        HibernateUtil.process(session -> session.delete(saleOrder));
		
	}
	
	
	
	

}
