package com.bk.view.site.manageSite;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.bk.model.ImportSite;

import jiconfont.icons.font_awesome.FontAwesome;
import jiconfont.swing.IconFontSwing;

@SuppressWarnings("serial")
public class SitePanel extends JPanel{
	
	private JPanel infoBox, productBox;
	
	static final Icon logoutIcon = IconFontSwing.buildIcon(FontAwesome.SIGN_OUT, 16);
	static final Icon plusIcon = IconFontSwing.buildIcon(FontAwesome.PLUS, 16);
	static final Icon penIcon = IconFontSwing.buildIcon(FontAwesome.PENCIL, 16);
	static final Icon deleteIcon = IconFontSwing.buildIcon(FontAwesome.TRASH, 16);
	static final Icon updateIcon = IconFontSwing.buildIcon(FontAwesome.BOOK, 16);
	static final Icon backIcon = IconFontSwing.buildIcon(FontAwesome.BACKWARD, 16);
	static final Icon searchIcon = IconFontSwing.buildIcon(FontAwesome.SEARCH, 16);
	static final Icon cancelIcon = IconFontSwing.buildIcon(FontAwesome.WINDOW_CLOSE, 16);
	
    private JButton backBtn = new JButton("Log out", logoutIcon);
    private JButton editInfoBtn = new JButton("Edit", penIcon);
	private JButton addProductBtn = new JButton("Add", plusIcon);
	private JButton deleteProductsBtn = new JButton("Delete", deleteIcon);
	private JCheckBox toggleCheckBox = new JCheckBox("Select All");
	private JTextField searchField = new JTextField();
	private JButton addFromFileBtn = new JButton("Load", updateIcon);
	
	//private ImportSite site = null;

    public SitePanel() {
        setLayout(new BorderLayout());
    }
    
    public void initSite(ImportSite site, JTable productTable) {
    	//this.site = site;
    	constructInfoBox(site);
    	constructProductBox(productTable);
    	add(productBox, BorderLayout.CENTER);
    }
    
    private JLabel siteIdLabel;
	private JLabel siteNameLabel;
    private JLabel daysAirLabel;
    private JLabel daysShipLabel;
    private GroupLayout layout;

	public void constructInfoBox(ImportSite site) {
		siteIdLabel = new JLabel("You are logging in as site " + site.getName() + " (ID: " + site.getId() + ")");
		siteNameLabel = new JLabel("Site Name: " + site.getName());
        daysAirLabel = new JLabel("Delivery days by air: " + site.getDays_delivery_air());
        daysShipLabel = new JLabel("Delivery days by ship: " + site.getDays_delivery_ship());
        
		if (infoBox != null) infoBox.removeAll();
		infoBox = new JPanel();
		infoBox.setBorder(new TitledBorder(null, "SITE INFORMATION", TitledBorder.CENTER, TitledBorder.CENTER, new Font("Times New Roman", Font.ITALIC + Font.BOLD, 30), Color.RED));
		infoBox.setBackground(Color.LIGHT_GRAY);
		layout = new GroupLayout(infoBox);
        infoBox.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        layout.setHorizontalGroup(	
        		layout.createSequentialGroup()
        			.addGroup(layout.createParallelGroup()
        					.addComponent(siteNameLabel)
			        		.addComponent(daysAirLabel)
			        		.addComponent(daysShipLabel)
			        		.addComponent(editInfoBtn)
			        )
        			.addGap(1200)
        			.addGroup(layout.createParallelGroup()
        					.addComponent(siteIdLabel)
        					.addComponent(backBtn)
        			)		
        );
		
        layout.setVerticalGroup(
	        	layout.createParallelGroup().
	        		addGroup(layout.createSequentialGroup()
			            	.addComponent(siteNameLabel)
			            	.addComponent(daysAirLabel)
			           		.addComponent(daysShipLabel)
			           		.addComponent(editInfoBtn)
		           	)
	        		.addGroup(layout.createSequentialGroup()
	        				.addComponent(siteIdLabel)
	        				.addComponent(backBtn)
	        		)
         );
        infoBox.setPreferredSize(new Dimension(300, 200));
		add(infoBox, BorderLayout.NORTH);
	}
	
	public void constructProductBox(JTable productTable) {
		productBox = new JPanel();
        productBox.setLayout(new BoxLayout(productBox, BoxLayout.Y_AXIS));
        productBox.add(searchBar());
		productBox.add(new JScrollPane(productTable));
		productBox.add(buttonBar());
	}
	
	private JPanel buttonBar() {
		JPanel panel = new JPanel();
		panel.add(addFromFileBtn);
		panel.add(addProductBtn);
		panel.add(deleteProductsBtn);
		panel.add(toggleCheckBox);
		return panel;
	}
	
	private JPanel searchBar() {
		JPanel bigPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));
		JPanel panel = new JPanel();		
		Icon searchIcon = IconFontSwing.buildIcon(FontAwesome.SEARCH, 14);
		JLabel searchLabel = new JLabel();
		searchLabel.setIcon(searchIcon);
		panel.add(searchLabel);
		searchField.setToolTipText("Search...");
		searchField.setPreferredSize(new Dimension(250, 30));
		panel.add(searchField);
		panel.setPreferredSize(new Dimension(300, 50));
		//panel.setBackground(Color.green);
		bigPanel.add(panel);
		bigPanel.setPreferredSize(new Dimension(300, 50));
		return bigPanel;
	}

	public JPanel getInfoBox() {
		return infoBox;
	}

	public JPanel getProductBox() {
		return productBox;
	}

    public JButton getBackBtn() {
        return backBtn;
    }
    
    public JButton getEditInfoBtn() {
		return editInfoBtn;
	}

	public JButton getAddProductBtn() {
		return addProductBtn;
	}
	
	public JButton getDeleteProductsBtn() {
		return deleteProductsBtn;
	}

	public JCheckBox getToggleCheckBox() {
		return toggleCheckBox;
	}
	
	public JTextField getSearchField() {
		return searchField;
	}

	public JButton getAddFromFileBtn() {
		return addFromFileBtn;
	}
	
    public JLabel getSiteIdLabel() {
		return siteIdLabel;
	}

	public JLabel getSiteNameLabel() {
		return siteNameLabel;
	}

	public JLabel getDaysAirLabel() {
		return daysAirLabel;
	}

	public JLabel getDaysShipLabel() {
		return daysShipLabel;
	}

}
