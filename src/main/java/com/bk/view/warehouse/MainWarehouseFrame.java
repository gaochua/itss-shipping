package com.bk.view.warehouse;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.bk.App;
import com.bk.utility.ListBean;
import com.bk.utility.WarehouseChangeScreenHelper;

import jiconfont.icons.font_awesome.FontAwesome;
import jiconfont.swing.IconFontSwing;

import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;

public class MainWarehouseFrame extends JFrame {

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public MainWarehouseFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("BK Shipping");

		  // full size
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screenSize.width, screenSize.height);
//		setBounds(100, 100, 600, 350);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("../main/ecommerce-icon.png")));

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JPanel pnlMenuWarehouse = new JPanel();
		pnlMenuWarehouse.setBackground(new Color(204,229,255));

		JPanel pnlMainScreen = new JPanel();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(pnlMenuWarehouse, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(pnlMainScreen, GroupLayout.DEFAULT_SIZE, 1070, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(pnlMainScreen, GroupLayout.DEFAULT_SIZE, 719, Short.MAX_VALUE)
						.addComponent(pnlMenuWarehouse, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addGap(0))
		);

		JPanel pnlHeadSalesOrder = new JPanel();
		pnlHeadSalesOrder.setBackground(Color.BLUE);

		JPanel pnlProcessingProduct = new JPanel();
		pnlProcessingProduct.setBackground(new Color(102,178,255));
		JLabel lblProcessingProduct = new JLabel("Processing Product");
		lblProcessingProduct.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblProcessingProduct.setBackground(new Color(102,178,255));
		lblProcessingProduct.setIcon(IconFontSwing.buildIcon(FontAwesome.HOURGLASS_START, 20));


		JPanel pnlWarehouseProduct = new JPanel();
		pnlWarehouseProduct.setBackground(new Color(102,178,255));
		JLabel lblWarehouseProduct = new JLabel("Warehouse Product");
		lblWarehouseProduct.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblWarehouseProduct.setBackground(new Color(102,178,255));
		lblWarehouseProduct.setIcon(IconFontSwing.buildIcon(FontAwesome.CHECK_CIRCLE, 20));
		
		JPanel pnlErrorProduct = new JPanel();
		pnlErrorProduct.setBackground(new Color(102,178,255));
		JLabel lblErrorProduct = new JLabel("Error Product");
		lblErrorProduct.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblErrorProduct.setBackground(new Color(102,178,255));
		lblErrorProduct.setIcon(IconFontSwing.buildIcon(FontAwesome.TIMES_CIRCLE, 20));
		
		JLabel lblHeadWarehouse = new JLabel("Manage Received Product");
		lblHeadWarehouse.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblHeadWarehouse.setForeground(Color.BLACK);
		lblHeadWarehouse.setIcon(IconFontSwing.buildIcon(FontAwesome.HOME, 50));

		
		JButton btnBack = new JButton("Log out");
		btnBack.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(btnBack);
				frame.setVisible(false);
				frame.dispose();
				App.mainFrame.setVisible(true);	
			}
		});

		GroupLayout gl_pnlWarehouseProduct = new GroupLayout(pnlWarehouseProduct);
		gl_pnlWarehouseProduct.setHorizontalGroup(
			gl_pnlWarehouseProduct.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlWarehouseProduct.createSequentialGroup()
					.addGap(42)
					.addComponent(lblWarehouseProduct)
					.addContainerGap(55, Short.MAX_VALUE))
		);
		gl_pnlWarehouseProduct.setVerticalGroup(
			gl_pnlWarehouseProduct.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlWarehouseProduct.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblWarehouseProduct, GroupLayout.DEFAULT_SIZE, 20, Short.MAX_VALUE)
					.addContainerGap())
		);
		pnlWarehouseProduct.setLayout(gl_pnlWarehouseProduct);

	
		GroupLayout gl_pnlErrorProduct = new GroupLayout(pnlErrorProduct);
		gl_pnlErrorProduct.setHorizontalGroup(
			gl_pnlErrorProduct.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_pnlErrorProduct.createSequentialGroup()
					.addGap(43)
					.addComponent(lblErrorProduct)
					.addContainerGap(85, Short.MAX_VALUE))
		);
		gl_pnlErrorProduct.setVerticalGroup(
			gl_pnlErrorProduct.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_pnlErrorProduct.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblErrorProduct, GroupLayout.DEFAULT_SIZE, 20, Short.MAX_VALUE)
					.addContainerGap())
		);
		pnlErrorProduct.setLayout(gl_pnlErrorProduct);
		
		GroupLayout gl_pnlMenuWarehouse = new GroupLayout(pnlMenuWarehouse);
		gl_pnlMenuWarehouse.setHorizontalGroup(
			gl_pnlMenuWarehouse.createParallelGroup(Alignment.TRAILING)
				.addComponent(pnlHeadSalesOrder, GroupLayout.PREFERRED_SIZE, 212, Short.MAX_VALUE)
				.addGroup(gl_pnlMenuWarehouse.createSequentialGroup()
					.addContainerGap()
					.addComponent(pnlProcessingProduct, GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_pnlMenuWarehouse.createSequentialGroup()
					.addContainerGap()
					.addComponent(pnlWarehouseProduct, GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_pnlMenuWarehouse.createSequentialGroup()
					.addContainerGap()
					.addComponent(pnlErrorProduct, GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_pnlMenuWarehouse.createSequentialGroup()
					.addGap(87)
					.addComponent(btnBack, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(68, Short.MAX_VALUE))
		);
		gl_pnlMenuWarehouse.setVerticalGroup(
			gl_pnlMenuWarehouse.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlMenuWarehouse.createSequentialGroup()
					.addComponent(pnlHeadSalesOrder, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(pnlProcessingProduct, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(pnlWarehouseProduct, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(pnlErrorProduct, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 464, Short.MAX_VALUE)
					.addComponent(btnBack, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);

		GroupLayout gl_pnlProcessingProduct = new GroupLayout(pnlProcessingProduct);
		gl_pnlProcessingProduct.setHorizontalGroup(
			gl_pnlProcessingProduct.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_pnlProcessingProduct.createSequentialGroup()
					.addGap(42)
					.addComponent(lblProcessingProduct)
					.addContainerGap(58, Short.MAX_VALUE))
		);
		gl_pnlProcessingProduct.setVerticalGroup(
			gl_pnlProcessingProduct.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_pnlProcessingProduct.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblProcessingProduct, GroupLayout.DEFAULT_SIZE, 20, Short.MAX_VALUE)
					.addContainerGap())
		);
		pnlProcessingProduct.setLayout(gl_pnlProcessingProduct);

		
		GroupLayout gl_pnlHeadSalesOrder = new GroupLayout(pnlHeadSalesOrder);
		gl_pnlHeadSalesOrder.setHorizontalGroup(
				gl_pnlHeadSalesOrder.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_pnlHeadSalesOrder.createSequentialGroup()
						.addContainerGap()
						.addComponent(lblHeadWarehouse, GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
						.addContainerGap())
				);
		gl_pnlHeadSalesOrder.setVerticalGroup(
				gl_pnlHeadSalesOrder.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlHeadSalesOrder.createSequentialGroup()
						.addContainerGap()
						.addComponent(lblHeadWarehouse, GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
						.addContainerGap())
				);
		pnlHeadSalesOrder.setLayout(gl_pnlHeadSalesOrder);
		pnlMenuWarehouse.setLayout(gl_pnlMenuWarehouse);
		contentPane.setLayout(gl_contentPane);
		
		
		WarehouseChangeScreenHelper helper = new WarehouseChangeScreenHelper(pnlMainScreen);
		helper.SetView(pnlProcessingProduct, lblProcessingProduct);
		
		List <ListBean> listItem = new ArrayList<>();
		listItem.add(new ListBean("ProcessingProduct", pnlProcessingProduct, lblProcessingProduct));
		listItem.add(new ListBean("WarehouseValidProduct", pnlWarehouseProduct, lblWarehouseProduct));
		listItem.add(new ListBean("WarehouseErrorProduct", pnlErrorProduct, lblErrorProduct));

		helper.setEvent(listItem);
		
	}
}
