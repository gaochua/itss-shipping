package com.bk.controller.site;

import javax.swing.JOptionPane;

import com.bk.App;
import com.bk.model.ImportSite;
import com.bk.model.Product;
import com.bk.model.SiteInstock;
import com.bk.model.Unit;
import com.bk.utility.HibernateUtil;
import com.bk.utility.ProductTableModel;
import com.bk.view.site.manageSite.AddProductForm;
import com.bk.view.site.manageSite.AddProductFrame;

public class AddProductController extends SiteController {

	public AddProductController() {
		sitePanel.getAddProductBtn().addActionListener(actionEvent -> {
			addProductFrame = new AddProductFrame();
			try {
				initAddProduct(site);
			} catch (InvalidDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			addProductFrame.pack();
			addProductFrame.setLocationRelativeTo(App.mainFrame);
        	addProductFrame.setVisible(true);
        });
	}
	
	private void initAddProduct(ImportSite site) throws InvalidDataException {
		addProductFrame.getCancelBtn().addActionListener(ae -> {
			addProductFrame.dispose();
		});
		
		AddProductForm form = addProductFrame.getForm();
		
		addProductFrame.getSubmitBtn().addActionListener(ae -> {
			int choice = JOptionPane.showConfirmDialog(null, "Confirm adding new product?", "UPDATE", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
			if (choice == JOptionPane.YES_OPTION) {
				String name = form.getContent(1);
				if (name.isEmpty()) {
					JOptionPane.showMessageDialog(null, "Product name can't be empty!", "ERROR", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				String description = form.getContent(3);
				
				Unit unit = form.getContent(5);
				
				Integer quantity = 0;
				try {
					quantity = form.getContent(7);
				}
				catch (Exception e) {
					JOptionPane.showMessageDialog(null, "quantity can't be empty!", "ERROR", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				
				if (hibernateGetSiteInstock(name, site.getName()) != null) {
					JOptionPane.showMessageDialog(null, "Product already exists!", "ERROR", JOptionPane.ERROR_MESSAGE);
					return;
				}
				else {
					try {
						addProduct(hibernateGetProduct(name, description, 1), quantity, unit, site);
					} catch (InvalidDataException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					table.setModel(new ProductTableModel(site));
					JOptionPane.showMessageDialog(null, "Add product successfully!", "SUCCESS", JOptionPane.INFORMATION_MESSAGE);
				}
			}
			addProductFrame.dispose();
		});
	}

	public static void addProduct(Product product, int quantity, Unit unit, ImportSite site) {
		SiteInstock instock = new SiteInstock(product, quantity, unit, site);
		HibernateUtil.process(session -> session.save(instock));
	}
	
}
