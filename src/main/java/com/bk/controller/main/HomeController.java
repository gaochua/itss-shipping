package com.bk.controller.main;

import com.bk.model.Role;
import javax.swing.JPanel;
import com.bk.App;
import com.bk.view.main.AbstractHomePanel;

public class HomeController extends AbstractHomeController{

    public HomeController(AbstractHomePanel homePanel, JPanel orderPanel,
                          JPanel salesPanel, JPanel sitePanel) {
        super(homePanel, orderPanel, salesPanel, sitePanel);
    }

    @Override
    public void setupPanels() {
    	AbstractLoginController loginController = new LoginController();
    	loginController.setPrevPanel(homePanel);
    	loginController.setupPanels();

    	homePanel.getOrderBtn().addActionListener(actionEvent -> {
    		loginController.setLoginRole(Role.ORDER_EMPLOYEE);
    		loginController.setNextPanel(orderPanel);
    		App.mainFrame.replacePanel(homePanel, loginController.getLoginPanel());
    	});
    	homePanel.getSiteBtn().addActionListener(actionEvent -> {
    		loginController.setLoginRole(Role.IMPORT_SITE_EMPLOYEE);
    		loginController.setNextPanel(sitePanel);
    		App.mainFrame.replacePanel(homePanel, loginController.getLoginPanel());
    	});
    	
    	homePanel.getSalesBtn().addActionListener(actionEvent -> {
    		loginController.setLoginRole(Role.SALES_EMPLOYEE);
    		App.mainFrame.replacePanel(homePanel, loginController.getLoginPanel());
    	});
    	homePanel.getWarehouseBtn().addActionListener(actionEvent -> {
    		loginController.setLoginRole(Role.WAREHOUSE_EMPLOYEE);
    		App.mainFrame.replacePanel(homePanel, loginController.getLoginPanel());
    	});

    }
}
