package com.bk.view.order;

import com.bk.utility.UIHelper;
import javax.swing.border.EmptyBorder;

import com.bk.model.*;
import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.Vector;

public class UpdateDeliveryPanel extends JPanel {
    private DeliveryOrder deliveryOrder;
    private JButton backBtn = new JButton("Cancel");
    private JButton submitBtn = new JButton("Update");
    private JTextField quantityField = new JTextField();;
    private JComboBox productBox;
    private JComboBox siteBox;
    private JComboBox unitBox  = new JComboBox(Unit.values());
    private JComboBox methodBox  = new JComboBox(DeliveryMethod.values());
    private String title = "Update order";

    public void setTitle(String title) {
        this.title = title;
    }
    
    private void setText(Vector<Product> productVector, Vector<ImportSite> importSiteVector) {
        if (deliveryOrder == null) {
            return;
        }
        quantityField.setText(Integer.toString(deliveryOrder.getQuantity()));
        Product orderProduct = deliveryOrder.getProduct();
        ImportSite orderSite = deliveryOrder.getSite();
        for (Product product : productVector) {
            System.out.println(product);
            if (product.equals(orderProduct)) {
                productBox.setSelectedItem(product);
                break;
            }
        }
        for (ImportSite site : importSiteVector) {
            System.out.println(site);
            if (site.equals(orderSite)) {
                siteBox.setSelectedItem(site);
                break;
            }
        }
        unitBox.setSelectedItem(deliveryOrder.getUnit());
        methodBox.setSelectedItem(deliveryOrder.getMethod());
    }

    public DeliveryOrder getDeliveryOrder() {
        return deliveryOrder;
    }

    public JTextField getQuantityField() {
        return quantityField;
    }

    public JComboBox getProductBox() {
        return productBox;
    }

    public JComboBox getSiteBox() {
        return siteBox;
    }

    public JComboBox getUnitBox() {
        return unitBox;
    }

    public JComboBox getMethodBox() {
        return methodBox;
    }

    public JButton getSubmitBtn() {
        return submitBtn;
    }

    public JButton getBackBtn() {
        return backBtn;
    }

    public void updateDisplay(DeliveryOrder deliveryOrder, List<Product> products, List<ImportSite> sites) {
        Vector<Product> productVector = new Vector<>(products);
        Vector<ImportSite> importSiteVector = new Vector<>(sites);
        this.deliveryOrder = deliveryOrder;
        removeAll();
        productBox  = new JComboBox(productVector );
        siteBox  = new JComboBox(importSiteVector);

        JPanel childPanel = new JPanel();
        childPanel.setLayout(new BorderLayout());
        childPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        JLabel titleLabel = new JLabel(title);
        UIHelper.setBoldLabel(titleLabel);
        childPanel.add(titleLabel, BorderLayout.PAGE_START);
        childPanel.add(createInputPanel(), BorderLayout.CENTER);
        childPanel.add(createButtonPanel(), BorderLayout.PAGE_END);
        add(childPanel);

        setText(productVector, importSiteVector);
    }

    private JPanel createInputPanel() {
        JPanel panel = new JPanel(new GridLayout(5, 2, 5, 5));
        JLabel quantity_label = new JLabel("Quantity:");
        JLabel product_label = new JLabel("Product:");
        JLabel site_label = new JLabel("Site:");
        JLabel unit_label = new JLabel("Unit:");
        JLabel method_label = new JLabel("Method:");
        
        panel.add(site_label);
        panel.add(siteBox);
        panel.add(product_label);
        panel.add(productBox);
        panel.add(quantity_label);
        panel.add(quantityField);
        panel.add(unit_label);
        panel.add(unitBox);
        panel.add(method_label);
        panel.add(methodBox);

        return panel;
    }

    private JPanel createButtonPanel() {
        JPanel panel = new JPanel();
        panel.add(submitBtn);
        panel.add(backBtn);
        return panel;
    }

}
