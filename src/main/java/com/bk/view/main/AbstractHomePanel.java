package com.bk.view.main;

import javax.swing.*;

public abstract class AbstractHomePanel extends JPanel {
    protected JButton siteBtn, salesBtn, orderBtn, warehouseBtn;

    public JButton getSiteBtn() {
        return siteBtn;
    }

    public JButton getSalesBtn() {
        return salesBtn;
    }

    public JButton getOrderBtn() {
        return orderBtn;
    }

    public JButton getWarehouseBtn() {
        return warehouseBtn;
    }
}
