package com.bk.view.sales;

import javax.swing.JPanel;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import com.bk.controller.sales.ManageSalesOrderController;

import jiconfont.icons.font_awesome.FontAwesome;
import jiconfont.swing.IconFontSwing;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SalesOrderPanel extends JPanel {
	private JTextField tfSearch;
	private JPanel pnlView;
	private JButton btnCreate;
	private JButton btnDelete;
	
//	private int orderId;

	/**
	 * Create the panel.
	 */
	public SalesOrderPanel() {
		
		initComponent();
		
		ManageSalesOrderController controller = new ManageSalesOrderController(pnlView, tfSearch, btnCreate, btnDelete);
		controller.viewSalesOrder();
		controller.createSalesOrder();
	
	}
	
	public void initComponent() {
		tfSearch = new JTextField();
		tfSearch.setColumns(10);
		
		pnlView = new JPanel();
		
		btnCreate = new JButton("Create Sale Order");
		btnCreate.setIcon(IconFontSwing.buildIcon(FontAwesome.PLUS, 16));
		
		btnDelete = new JButton("Delete Sale Order");
		btnDelete.setIcon(IconFontSwing.buildIcon(FontAwesome.ERASER, 16));
		btnDelete.setEnabled(false);
		
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(pnlView, GroupLayout.DEFAULT_SIZE, 624, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(tfSearch, GroupLayout.PREFERRED_SIZE, 328, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(btnDelete, GroupLayout.PREFERRED_SIZE, 164, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnCreate, GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(tfSearch, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnCreate, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnDelete, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(pnlView, GroupLayout.DEFAULT_SIZE, 231, Short.MAX_VALUE)
					.addContainerGap())
		);
		setLayout(groupLayout);
		
		
	}
}
