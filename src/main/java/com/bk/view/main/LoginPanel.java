package com.bk.view.main;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import com.bk.utility.UIHelper;

import jiconfont.icons.font_awesome.FontAwesome;
import jiconfont.swing.IconFontSwing;

import java.awt.*;

public class LoginPanel extends AbstractLoginPanel {
    public LoginPanel() {
        usernameField = new JTextField(15);
        passwordField = new JPasswordField(15);
        submitBtn = new JButton("Submit", IconFontSwing.buildIcon(FontAwesome.CHECK, 16));
        backBtn = new JButton("Back", IconFontSwing.buildIcon(FontAwesome.BACKWARD, 16));

        JLabel loginLabel = new JLabel("Login");
        UIHelper.setBoldLabel(loginLabel);
        JPanel childPanel = new JPanel();

        childPanel.setLayout(new BorderLayout());
        childPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        childPanel.add(loginLabel, BorderLayout.PAGE_START);
        childPanel.add(createInputPanel(), BorderLayout.CENTER);
        childPanel.add(createButtonPanel(), BorderLayout.PAGE_END);
        add(childPanel);

    }

    private JPanel createInputPanel() {
        JPanel panel = new JPanel(new GridLayout(2, 2, 5, 5));
        panel.add(new JLabel("Username"));
        panel.add(usernameField);
        panel.add(new JLabel("Password"));
        panel.add(passwordField);
        return panel;
    }

    private JPanel createButtonPanel() {
        JPanel panel = new JPanel();
        panel.add(submitBtn);
        panel.add(backBtn);
        return panel;
    }
}
