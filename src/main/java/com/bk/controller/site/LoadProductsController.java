package com.bk.controller.site;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.bk.model.Product;
import com.bk.model.Unit;
import com.bk.utility.ProductTableModel;

public class LoadProductsController extends SiteController {
	
	public LoadProductsController() {
		sitePanel.getAddFromFileBtn().addActionListener(ae -> {
			JFileChooser chooser = new JFileChooser();
		    FileNameExtensionFilter filter = new FileNameExtensionFilter("Text files", "txt");
		    chooser.setFileFilter(filter);
		    int returnVal = chooser.showOpenDialog(null);
		    if(returnVal == JFileChooser.APPROVE_OPTION) {
		    	String filename = chooser.getSelectedFile().getAbsolutePath();
		    	try {
					AddProductsFromFile(filename);
				} catch (InvalidDataException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    	table.setModel(new ProductTableModel(site));
		    	JOptionPane.showMessageDialog(null, "Load products successfully!", "SUCCESS", JOptionPane.INFORMATION_MESSAGE);
		    }
		});
	}
	
	public void AddProductsFromFile(String filename) throws InvalidDataException {
		Path file = Paths.get(filename);
		try (InputStream in = Files.newInputStream(file);
			    BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
			    String line = null;
			    while ((line = reader.readLine()) != null) {
			        addProductFromLine(line);
			    }
			} catch (IOException x) {
			    System.err.println(x);
			}
	}
	
	public void addProductFromLine(String line) throws InvalidDataException {
		//System.out.println(line);
		String s[] = line.split(";");
		Product product = hibernateGetProduct(s[0], s[1], 2);
		int quantity = Integer.parseInt(s[3]);
		Unit unit = Unit.valueOf(s[2]);
		AddProductController.addProduct(product, quantity, unit, site);
	}

}
