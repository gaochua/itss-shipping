package com.bk.utility;

import java.util.List;

import javax.swing.table.DefaultTableModel;

import com.bk.model.DeliveryOrder;
import com.bk.model.SalesOrderItem;

public class WarehouseTableModel {
	
	public DefaultTableModel setTableProduct(List <DeliveryOrder> listItem, String[] listColumn) {
		DefaultTableModel dtm = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int rowIndex, int colIndex) {
				return false;
			}
		};
		dtm.setColumnIdentifiers(listColumn);
		Object obj[] = null;
		
		DeliveryOrder deliveryOrder = null;
//		SalesOrderItem salesOrderItem = null;
		int columns = listColumn.length;
		int rows = listItem.size();
		if (rows > 0) {
			for (int i = 0; i < rows; i++) {
				deliveryOrder = listItem.get(i);
	            obj = new Object[columns];
				obj[0] = (deliveryOrder.getId());
				obj[1] = deliveryOrder.getProduct().getId();
				obj[2] = deliveryOrder.getProduct().getName();
				obj[3] = deliveryOrder.getUnit();
				obj[4] = deliveryOrder.getQuantity();
				obj[5] = deliveryOrder.getQuantityReceived();

	            dtm.addRow(obj);
			}
		}
		return dtm;
	}

}
