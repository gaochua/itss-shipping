package com.bk.view.sales;

import javax.swing.*;

public class SalesPanel extends JPanel{
    private JButton backBtn = new JButton("Back");

    public SalesPanel() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(new JLabel("sales panel"));
        add(backBtn);
    }

    public JButton getBackBtn() {
        return backBtn;
    }

}
