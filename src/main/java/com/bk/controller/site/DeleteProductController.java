package com.bk.controller.site;

import java.util.List;

import javax.swing.JOptionPane;

import com.bk.model.Product;
import com.bk.model.SiteInstock;
import com.bk.utility.HibernateUtil;
import com.bk.utility.ProductTableModel;

public class DeleteProductController extends SiteController{
	
	public DeleteProductController() {
		sitePanel.getDeleteProductsBtn().addActionListener(actionEvent -> {
        	List<SiteInstock> selected = ((ProductTableModel) table.getModel()).getSelectedCheckBoxes();
        	if (selected.isEmpty()) {
        		JOptionPane.showMessageDialog(null, "No product is selected", "ERROR", JOptionPane.ERROR_MESSAGE);
        	}
        	else {
        		int choice = JOptionPane.showConfirmDialog(null, "Confirm deleting selected products?", "DELETE PRODUCTS", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        		if (choice == JOptionPane.YES_OPTION) {
        			for (SiteInstock instock : selected) {
        				deleteProduct(instock);
        			}
        			table.setModel(new ProductTableModel(site));
        			JOptionPane.showMessageDialog(null, "Delete product successfully!", "SUCCESS", JOptionPane.INFORMATION_MESSAGE);
        		}
        		table.updateUI();
        	}
        });
	}
	
	public void deleteProduct(SiteInstock instock) {
		Product product = instock.getProduct();
		HibernateUtil.process(session -> session.remove(instock));
		@SuppressWarnings("unchecked")
		List<SiteInstock> instocks = HibernateUtil.get(session -> session.createQuery("From SiteInstock Where product.name = :product").setParameter("product", product.getName()).getResultList());
		if (instocks.isEmpty()) {
			HibernateUtil.process(session -> session.remove(product));
		}
	}

}
