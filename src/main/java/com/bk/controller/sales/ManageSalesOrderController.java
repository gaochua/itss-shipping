package com.bk.controller.sales;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.bk.model.SalesOrder;
import com.bk.utility.ISalesOrderService;
import com.bk.utility.SalesOrderServiceImp;
import com.bk.utility.SalesOrderTableModel;
import com.bk.utility.SearchEvent;
import com.bk.utility.UIHelper;
import com.bk.view.sales.MainSalesFrame;
import com.bk.view.sales.MainSalesItemFrame;
import com.bk.view.sales.SalesOrderPanel;

public class ManageSalesOrderController {
	
	private JPanel pnlView;
	private JTextField tfSearch;
	private JButton btnCreate;
	private JButton btnDelete;
	
	private ISalesOrderService salesOrderService = null;
	private final String[] listColumn = {"ID", "User", "Number of Sale Items"};
	
	private TableRowSorter<TableModel>  rowSorter = null;	
	  
	public ManageSalesOrderController(JPanel pnlView, JTextField tfSearch, JButton btnCreate, JButton btnDelete) {
		super();
		this.pnlView = pnlView;
		this.tfSearch = tfSearch;
		this.btnCreate = btnCreate;
		this.btnDelete = btnDelete;
		this.salesOrderService = new SalesOrderServiceImp();
	}
	
	public void closeMainSalesFrame() {
		JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(pnlView);
		frame.setVisible(false);
		frame.dispose();
	}
	
	public void viewSalesOrder() {
		List <SalesOrder> listItem = salesOrderService.getList();
		
		DefaultTableModel model = new SalesOrderTableModel().setTableSalesOrder(listItem, listColumn);
		JTable table = new JTable(model);
		
		rowSorter = new TableRowSorter<TableModel>(table.getModel());
		table.setRowSorter(rowSorter);
		
		
		//add Search Event
		SearchEvent searchEvent = new SearchEvent();
		searchEvent.setSearchEvent(tfSearch, rowSorter);
		
		
		//add Event view a sales order
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				
				if (e.getClickCount() == 2 && table.getSelectedRow() != -1) {
					//view Sale Order
					int selectedRowIndex = table.getSelectedRow();
					selectedRowIndex = table.convertRowIndexToModel(selectedRowIndex);
					//System.out.println(selectedRowIndex);
					int orderId = (int) model.getValueAt(selectedRowIndex, 0);
					MainSalesItemFrame frame = new MainSalesItemFrame(orderId);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
					
					closeMainSalesFrame();
					
				} else if (e.getClickCount() == 1 && table.getSelectedRow() != -1){
					// delete Sale Order
					btnDelete.setEnabled(true);
					int selectedRowIndex = table.getSelectedRow();
					selectedRowIndex = table.convertRowIndexToModel(selectedRowIndex);
					int orderId = (int) model.getValueAt(selectedRowIndex, 0);
					deleteSaleOrder(orderId);
					
				} 
			}
		});  		

		// design
		table.getTableHeader().setFont(new Font("Arial", Font.BOLD, 14));
		table.getTableHeader().setPreferredSize(new Dimension(100, 50));
		table.setRowHeight(50);
		table.validate();
		table.repaint();
        
        JScrollPane scroll = new JScrollPane();
        scroll.getViewport().add(table);
        scroll.setPreferredSize(new Dimension(1350, 400));
        pnlView.removeAll();
        pnlView.setLayout(new CardLayout());
        pnlView.add(scroll);
        pnlView.validate();
        pnlView.repaint();
		
	}
	
	public void createSalesOrder() {
		btnCreate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				SalesOrder salesOrder = new SalesOrder();
				int orderId = salesOrderService.createSalesOrder(salesOrder);
				MainSalesItemFrame frame = new MainSalesItemFrame(orderId);
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
//				System.out.println(salesOrder.getId());
				
				closeMainSalesFrame();
				
			}
		});
	}

	public void deleteSaleOrder(int orderId) {
		btnDelete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
//				ISalesOrderService orderService = new SalesOrderServiceImp();
				SalesOrder order = salesOrderService.getById(orderId);
				salesOrderService.deleteSalesOrder(order);
				UIHelper.showSuccessDialog("Delete Successfully!");
				closeMainSalesFrame();
				
				new MainSalesFrame().setVisible(true);
			}
		});
		
	}

}
