package com.bk.controller.site;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.bk.model.Product;

@RunWith(Parameterized.class)
public class HibernateGetProductTest {

	String name;
	String description;
	int level;
	int returnType;
	
	public HibernateGetProductTest(String name, String description, int level, int returnType) {
		//super();
		this.name = name;
		this.description = description;
		this.level = level;
		this.returnType = returnType;
	}

	@Parameters(name = "{index}: testGetSiteInstock({0}, {1}, {2}) return type = {3}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"rice", "just rice", 1, 0},
                {"motorbike", "big bike", 0, 2},
                {"kitkat", "japanese candy", -1, 0},
                {"kitkat", "korean candy", 1, 1},
                {"water", "hot water", 200, 0}
        });
    }
	
	@Test
	public void doTest() {
		if (returnType == 0) {
			Product product = null;
			try {
				product = SiteController.hibernateGetProduct(name, description, level);
			} catch (InvalidDataException e) {
				assertTrue(false);
			}
			assertEquals(description, product.getDescription());
			assertEquals(name, product.getName());
		}
		else if (returnType == 1) {
			Product product = null;
			try {
				product = SiteController.hibernateGetProduct(name, description, level);
			} catch (InvalidDataException e) {
				assertTrue(false);
			}
			assertNotEquals(description, product.getDescription());
			assertEquals(name, product.getName());
		}
		else {
			try {
				SiteController.hibernateGetProduct(name, description, level);
			} catch (Exception e) {
				assertTrue(true);
				return;
			}
			assertTrue(false);
		}
	}

}
