package com.bk.controller.order;

import com.bk.App;
import com.bk.utility.HibernateUtil;
import com.bk.utility.UIHelper;
import com.bk.model.*;
import com.bk.view.order.AskInstockPanel;
import com.bk.view.order.OverseasOrderPanel;
import com.bk.view.site.ChooseSiteStep1Panel;
import com.bk.view.site.ChooseSiteStep2Panel;
import com.bk.view.site.FeedbackInstockPanel;
import com.bk.view.site.InstockPanel;

import java.io.IOException;
import java.util.List;

public class InstockController {
    private OverseasOrderPanel orderPanel;
    private InstockPanel instockPanel = new InstockPanel();
    private AskInstockPanel askInstockPanel;
    private FeedbackInstockPanel feedbackInstockPanel;
    private ChooseSiteStep1Panel chooseSiteStep1Panel;
    private ChooseSiteStep2Panel chooseSiteStep2Panel;

    List<SalesOrder> salesOrders;
    List<ImportSite> sites;
    List<SiteInstock> instocks;
    List<Product> products;
    List<DeliveryOrder> orders;
    List<SalesOrderItem> salesOrderItems;
    
    public InstockController(OverseasOrderPanel orderPanel) {
        this.orderPanel = orderPanel;
    }

    public void init(List<SalesOrder> salesOrders, List<ImportSite> sites, List<SiteInstock> instocks,
            List<Product> products, List<DeliveryOrder> orders, List<SalesOrderItem> items) {
        this.salesOrders = salesOrders;
        this.sites = sites;
        this.instocks = instocks;
        this.products = products;
        this.orders = orders;
        this.salesOrderItems = items;

        initViewPanel();

    }

    private void initViewPanel() {
        instockPanel.setSalesOrderList(salesOrders);
        instockPanel.getBackBtn().addActionListener(ae -> App.mainFrame.replacePanel(instockPanel, orderPanel));

        instockPanel.getInstockBtn().addActionListener(actionEvent -> {
            int index = instockPanel.getSelectedRowIndex();
            if (index == -1) {
                UIHelper.showErrorDialog("Invalid operation!");
                return;
            }
            this.initAskInstockPanel(salesOrders.get(index));
        });

        instockPanel.getChooseSiteBtn().addActionListener(actionEvent -> {
            int index = instockPanel.getSelectedRowIndex();
            if (index == -1) {
                UIHelper.showErrorDialog("Invalid operation!");
                return;
            }
            this.initChooseSiteStep1Panel(salesOrders.get(index));
        });

        App.mainFrame.replacePanel(orderPanel, instockPanel);

    }

    private void initChooseSiteStep1Panel(SalesOrder salesOrder) {

        chooseSiteStep1Panel = new ChooseSiteStep1Panel();
        chooseSiteStep1Panel.setSalesOrder(salesOrder);
        chooseSiteStep1Panel.setSalesOrderItems();
        chooseSiteStep1Panel.getBackBtn()
                .addActionListener(ae -> App.mainFrame.replacePanel(chooseSiteStep1Panel, instockPanel));
        chooseSiteStep1Panel.getNextBtn().addActionListener(ae -> {
            try {
                this.initChooseSiteStep2Panel(salesOrder);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            App.mainFrame.replacePanel(chooseSiteStep1Panel, chooseSiteStep2Panel);
        });

        App.mainFrame.replacePanel(instockPanel, chooseSiteStep1Panel);
    }

    private void initChooseSiteStep2Panel(SalesOrder salesOrder) throws IOException {

        chooseSiteStep2Panel = new ChooseSiteStep2Panel();
        chooseSiteStep2Panel.setSalesOrder(salesOrder);
        chooseSiteStep2Panel.setSalesOrderItems();
        chooseSiteStep2Panel.setAllSites(sites);
        chooseSiteStep2Panel.setAllOrders();
        chooseSiteStep2Panel.getBackBtn().addActionListener(ae -> App.mainFrame.replacePanel(chooseSiteStep2Panel, instockPanel));
        chooseSiteStep2Panel.getSubmitBtn().addActionListener(ae -> {
            for(DeliveryOrder order: chooseSiteStep2Panel.getAllOrders()) {
                HibernateUtil.process(session -> session.save(order));
                orders.add(order);
            }
            salesOrders.remove(salesOrder);
            HibernateUtil.process(session -> session.update(salesOrders));
            App.mainFrame.replacePanel(chooseSiteStep2Panel, instockPanel);
                
        });
                
        App.mainFrame.replacePanel(chooseSiteStep1Panel, chooseSiteStep2Panel);
    }

    private void initAskInstockPanel(SalesOrder salesOrder) {

        askInstockPanel = new AskInstockPanel();
        askInstockPanel.setSalesOrder(salesOrder);
        askInstockPanel.setAllSites(sites);
        askInstockPanel.setSuitableSites();
       
        askInstockPanel.getViewBtn().addActionListener(ae -> {
            List<ImportSite> selectedSites = askInstockPanel.getSelectedSites();
            if(selectedSites == null) {
                UIHelper.showErrorDialog("Invalid operation!");
                return;
            }
            this.initFeedbackInstockPanel(selectedSites, salesOrder);
            App.mainFrame.replacePanel(askInstockPanel, feedbackInstockPanel);
        });
        askInstockPanel.getSubmitBtn().addActionListener(ae -> App.mainFrame.replacePanel(askInstockPanel, instockPanel));
        App.mainFrame.replacePanel(instockPanel, askInstockPanel);
    }

    private void initFeedbackInstockPanel(List<ImportSite> selectedSites, SalesOrder salesOrder) {

        feedbackInstockPanel = new FeedbackInstockPanel();
        feedbackInstockPanel.setSalesOrder(salesOrder);
        feedbackInstockPanel.setSelectedSites(selectedSites);
        feedbackInstockPanel.getExitBtn().addActionListener(ae -> App.mainFrame.replacePanel(feedbackInstockPanel, askInstockPanel));
        App.mainFrame.replacePanel(askInstockPanel, feedbackInstockPanel);
    }

    
}
