package com.bk.controller.site;

import java.util.List;

import javax.swing.JTable;

import com.bk.App;
import com.bk.model.ImportSite;
import com.bk.model.Product;
import com.bk.model.SiteInstock;
import com.bk.utility.HibernateUtil;
import com.bk.utility.ProductTableModel;
import com.bk.view.main.AbstractHomePanel;
import com.bk.view.site.manageSite.AddProductFrame;
import com.bk.view.site.manageSite.EditSiteInfoFrame;
import com.bk.view.site.manageSite.ModifyProductFrame;
import com.bk.view.site.manageSite.SitePanel;

@SuppressWarnings({"unchecked"})
public class SiteController {
    
	static protected AbstractHomePanel homePanel;
    static protected SitePanel sitePanel;
    static protected EditSiteInfoFrame editSiteInfoFrame;
    static protected AddProductFrame addProductFrame;
    static protected ModifyProductFrame modifyProductFrame;
    
    static protected EditSiteInfoController editSiteInfoController;
    static protected AddProductController addProductController;
    static protected DeleteProductController deleteProductController;
    static protected ModifyProductController modifyProductController;
    static protected SearchProductController searchProductController;
    static protected LoadProductsController loadProductsController;
    
    static protected ImportSite site;
    static protected JTable table;
    
    public SiteController() {
    	//do nothing
    }

	public SiteController(AbstractHomePanel homePanel, SitePanel sitePanel, ImportSite site) {
		this.site = site;
        SiteController.homePanel = homePanel;
        SiteController.sitePanel = sitePanel;
        
        initTable();
        initButtons();
        editSiteInfoController = new EditSiteInfoController();
        addProductController = new AddProductController();
        deleteProductController = new DeleteProductController();
        modifyProductController = new ModifyProductController();
        searchProductController = new SearchProductController(); 
        loadProductsController = new LoadProductsController();
    }
	
	private void initTable() {
		ProductTableModel tableModel = new ProductTableModel(site);
        table = new JTable(tableModel);
        table.setAutoCreateRowSorter(true);
        table.getRowSorter().toggleSortOrder(ProductTableModel.MERCHANDISE_CODE_COLUMN);
        sitePanel.initSite(site, table);
	}

	private void initButtons() {
		sitePanel.getBackBtn().addActionListener(actionEvent -> App.mainFrame.replacePanel(sitePanel, homePanel));
        sitePanel.getToggleCheckBox().addItemListener(ae -> {
        	if (sitePanel.getToggleCheckBox().isSelected()) {
        		sitePanel.getToggleCheckBox().setText("Unselect All");
        		for (int i = 0; i < table.getRowCount(); ++i) {
        			table.setValueAt(true, i, ProductTableModel.SELECT_COLUMN);
        		}
        	}
        	else {
        		sitePanel.getToggleCheckBox().setText("Select All");
        		for (int i = 0; i < table.getRowCount(); ++i) {
        			table.setValueAt(false, i, ProductTableModel.SELECT_COLUMN);
        		}
        	}
        	table.updateUI();
        });
	}
	
	public List<ImportSite> hibernateGetSites() {
		List<ImportSite> sitesList = (List<ImportSite>) HibernateUtil.get(session -> session.createQuery("From ImportSite").list());
		return sitesList;
	}
	
	public ImportSite hibernateGetSite(int index) {
		return hibernateGetSites().get(index);
	}
	
	/**
	 * Returns a {@link Product} where Product.getName().contentEquals({@code name}).
	 * <p>
	 * If level < 1: if no such product is found, throws InvalidDataException.
	 * <p>
	 * If level >= 1 : if no such product is found, creates a {@code Product(name, description)}.
	 * <p>
	 * If level > 1: If such product is found, changes the description of that product to {@code description}
	 */
	public static Product hibernateGetProduct(String name, String description, int level) 
			throws InvalidDataException {
		List<Product> products = HibernateUtil.get(session -> session
				.createQuery("From Product Where name=:name")
				.setParameter("name", name)
				.getResultList());
		Product product;
		if (products.isEmpty()) {
			if (level >= 1) {
				product = new Product(name, description);
				HibernateUtil.process(session -> session.save(product));
			}
			else {
				throw new InvalidDataException("Product name: '" + name + "' does not exist.");
			}
		}
		else {
			product = products.get(0);
			if (!product.getDescription().contentEquals(description) && level > 1) {
				product.setDescription(description);
				HibernateUtil.process(session -> session.update(product));
			}
		}
		return product;
	}
	
	public static SiteInstock hibernateGetSiteInstock(String productName, String siteName) {
		List<SiteInstock> instocks = HibernateUtil
				.get(session -> session
						.createQuery("From SiteInstock "
								+ "Where product.name = :product "
								+ "and site.name = :site")
				.setParameter("product", productName)
				.setParameter("site", siteName)
				.getResultList());
		
		if (instocks.isEmpty()) {
			return null;
		}
		else {
			return instocks.get(0);
		}
	}

	public SitePanel getSitePanel() {
		return sitePanel;
	}

	public static ImportSite getSite() {
		return site;
	}

	public static void setSite(ImportSite site) {
		SiteController.site = site;
	}
	
}
