package com.bk.model;


import javax.persistence.*;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "import_site")
public class ImportSite implements DataModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, unique = true)
    private String name;

    public ImportSite(String name, int days_delivery_air, int days_delivery_ship) {
        this.name = name;
        this.days_delivery_air = days_delivery_air;
        this.days_delivery_ship = days_delivery_ship;
    }

    @Override
    public String toString() {
        return name;
    }

    public ImportSite() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<SiteInstock> getInstocks() {
        return instocks;
    }

    public void setInstocks(Set<SiteInstock> instocks) {
        this.instocks = instocks;
    }

    public int getDays_delivery_air() {
        return days_delivery_air;
    }

    public void setDays_delivery_air(int days_delivery_air) {
        this.days_delivery_air = days_delivery_air;
    }

    public int getDays_delivery_ship() {
        return days_delivery_ship;
    }

    public void setDays_delivery_ship(int days_delivery_ship) {
        this.days_delivery_ship = days_delivery_ship;
    }

    public Set<DeliveryOrder> getOrders() {
        return orders;
    }

    public void setOrders(Set<DeliveryOrder> orders) {
        this.orders = orders;
    }

    @OneToMany(mappedBy="site", fetch = FetchType.EAGER)
    private Set<SiteInstock> instocks;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImportSite site = (ImportSite) o;
        return id == site.id &&
                name.equals(site.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Column(nullable = false)
    private int days_delivery_air = 0;

    @Column(nullable = false)
    private int days_delivery_ship = 0;

    @OneToMany(mappedBy="site")
    private Set<DeliveryOrder> orders;
}
