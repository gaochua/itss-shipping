package com.bk.controller.site;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.bk.model.SiteInstock;
import com.bk.controller.site.SiteController;

@RunWith(value = Parameterized.class)
public class HibernateGetSiteInstockTest {
	
	String productName;
	String siteName;
	boolean expectNull;
	
	public HibernateGetSiteInstockTest(String productName, String siteName, boolean expectNull) {
		this.productName = productName;
		this.siteName = siteName;
		this.expectNull = expectNull;
	}
	
	@Parameters(name = "{index}: testGetSiteInstock({0}, {1}) return null = {2}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"kitkat", "amazon", false},
                {"water", "amazon", true},
                {"chocoie", "shopee", true},
                {"motorbike", "shopee", true},
                {"car", "google", true}
        });
    }
	
	@Test
	public void doTest() {
		SiteInstock instock = SiteController.hibernateGetSiteInstock(productName, siteName);
		if (expectNull) {
			assertNull(instock);
		}
		else {
			assertNotNull(instock);
		}
	}
	
}
