package com.bk.view.warehouse;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.TitledBorder;

import com.bk.controller.warehouse.WarehouseController;
import com.bk.model.DeliveryOrder;
import com.bk.model.Unit;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.border.EtchedBorder;
import java.awt.Color;
import java.awt.Toolkit;

public class WarehouseForm extends JFrame {

	private JPanel contentPane;
	private JTextField tfDeliveryOrderId;
	private JTextField tfProduct;
	private JTextField tfQuantityOrder;
	private JComboBox cbUnit;
	
	private JButton btnSave;
	
	private JLabel lblMessage;
	private JPanel panel;
	private JTextField tfQuantityReceived;


	/**
	 * Create the frame.
	 */
	public WarehouseForm(DeliveryOrder deliveryOrder) {
		setResizable(false);
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 350);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("../main/ecommerce-icon.png")));

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		initComponent();
		
		WarehouseController controller = new WarehouseController(tfDeliveryOrderId, tfProduct, tfQuantityOrder, tfQuantityReceived, cbUnit, btnSave, lblMessage);
		controller.setEventView(deliveryOrder);
		controller.setEventCheck(deliveryOrder);
		
		
	}
	
	
	public void initComponent() {
		btnSave = new JButton("Save");
		
		panel = new JPanel();
		panel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "Product Information", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		lblMessage = new JLabel("");
		
		JLabel lblDelieryOrderId = new JLabel("Delivery Order ID");
		JLabel lblProductName = new JLabel("Product Name:");
		JLabel lblQuantityOder = new JLabel("Quantity Order:");
		JLabel lblUnit = new JLabel("Unit:");
		
		tfDeliveryOrderId = new JTextField();
		tfDeliveryOrderId.setEditable(false);
		tfDeliveryOrderId.setColumns(10);
		
		tfQuantityOrder = new JTextField();
		tfQuantityOrder.setEditable(false);
		tfQuantityOrder.setColumns(10);
		cbUnit = new JComboBox(Unit.values());
		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(18)
					.addComponent(lblMessage, GroupLayout.PREFERRED_SIZE, 247, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 128, Short.MAX_VALUE)
					.addComponent(btnSave, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(1))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSave)
						.addComponent(lblMessage))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE))
		);
		
		tfProduct = new JTextField();
		tfProduct.setEditable(false);
		tfProduct.setColumns(10);
		
		JLabel lblQuantityReceived = new JLabel("Quantity Received:");
		
		tfQuantityReceived = new JTextField();
		tfQuantityReceived.setColumns(10);
		
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
								.addComponent(lblQuantityOder, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lblProductName, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addGap(18))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblDelieryOrderId, GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)))
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(tfDeliveryOrderId, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(tfProduct, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(tfQuantityOrder, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblQuantityReceived)
						.addComponent(lblUnit, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(cbUnit, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(tfQuantityReceived, GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE))
					.addContainerGap(32, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(36)
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(tfDeliveryOrderId, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblDelieryOrderId))
							.addGap(38)
							.addComponent(tfProduct, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(32)
							.addComponent(tfQuantityOrder, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblProductName)
								.addComponent(lblUnit)
								.addComponent(cbUnit, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(32)
							.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblQuantityOder)
								.addComponent(lblQuantityReceived)
								.addComponent(tfQuantityReceived, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(83, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);

	}
}
