package com.bk.utility;

import java.util.List;

import javax.swing.table.DefaultTableModel;

import com.bk.model.SalesOrder;


public class SalesOrderTableModel {
			
	public DefaultTableModel setTableSalesOrder(List <SalesOrder> listItem, String[] listColumn) {
		DefaultTableModel dtm = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int rowIndex, int colIndex) {
				return false;
			}
		};
		dtm.setColumnIdentifiers(listColumn);
		Object obj[] = null;
		
		SalesOrder salesOrder = null;
		int columns = listColumn.length;
		int rows = listItem.size();
		if (rows > 0) {
			for (int i = 0; i < rows; i++) {
				salesOrder = listItem.get(i);
	            obj = new Object[columns];
				obj[0] = salesOrder.getId();
				obj[1] = salesOrder.getUser().getName();
				obj[2] = salesOrder.getItems().size();
//				obj[2] = Integer.toString(salesOrder.getItems().size());
	            dtm.addRow(obj);
			}
		}
		return dtm;
	}

}
