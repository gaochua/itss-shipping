package com.bk.view.site.manageSite;


import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.bk.model.ImportSite;

@SuppressWarnings("serial")
public class EditSiteInfoFrame extends JFrame {
	
	private EditSiteInfoForm form = new EditSiteInfoForm();
	private JButton cancelBtn = new JButton("Cancel", SitePanel.cancelIcon);
	private JButton submitBtn = new JButton("Submit", SitePanel.updateIcon);

	public EditSiteInfoFrame(ImportSite site) {
		this.setTitle("Update Site Information");
		setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		setMinimumSize(new Dimension(200, 300));
		
		JPanel formPanel = new JPanel();
		formPanel.add(form);
		formPanel.setAlignmentX(CENTER_ALIGNMENT);
		add(formPanel);
		
		JPanel btnsPanel = new JPanel();
		btnsPanel.add(submitBtn);
		btnsPanel.add(cancelBtn);
		add(btnsPanel);
	}
	
	public EditSiteInfoForm getForm() {
		return form;
	}

	public JButton getCancelBtn() {
		return cancelBtn;
	}

	public JButton getSubmitBtn() {
		return submitBtn;
	}

}
