package com.bk.view.order;

import com.bk.App;
import com.bk.model.DeliveryOrder;

import jiconfont.icons.font_awesome.FontAwesome;
import jiconfont.swing.IconFontSwing;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.awt.*;
import java.util.List;

public class ViewDeliveryPanel extends JPanel {
    private JButton backBtn = new JButton("Back");
    private JButton createBtn = new JButton("Create");
    private JButton deleteBtn = new JButton("Delete");
    private JButton updateBtn = new JButton("Update");
    private JScrollPane scrollPane = new JScrollPane();
    private JPanel childPanel = new JPanel();
    private JTable table;
    private DefaultTableModel tableModel;
    private List<DeliveryOrder> deliveryOrderList;

    public void setDeliveryOrderList(List<DeliveryOrder> deliveryOrderList) {
        this.deliveryOrderList = deliveryOrderList;
        showData();
    }

    private void setupButtons() {
        Icon plusIcon = IconFontSwing.buildIcon(FontAwesome.PLUS, 16);
        Icon deleteIcon = IconFontSwing.buildIcon(FontAwesome.TRASH, 16);
        Icon updateIcon = IconFontSwing.buildIcon(FontAwesome.BOOK, 16);
        Icon backIcon = IconFontSwing.buildIcon(FontAwesome.BACKWARD, 16);
        createBtn.setIcon(plusIcon);
        deleteBtn.setIcon(deleteIcon);
        updateBtn.setIcon(updateIcon);
        backBtn.setIcon(backIcon);
    }

    public ViewDeliveryPanel() {
        setLayout(new BorderLayout());
        setupButtons();
    }

    public JButton getBackBtn() {
        return backBtn;
    }

    private void showData() {
        childPanel.setLayout(new FlowLayout());
        
        removeAll();
        childPanel.removeAll();
        revalidate();

        String[] columnNames = {"ID", "Site", "Product", "Quantity", "Unit", "Delivery means", "Created date"};
        Object[][] values = new Object[deliveryOrderList.size()][7];
        for (int i = 0; i < deliveryOrderList.size(); i++) {
            DeliveryOrder order = deliveryOrderList.get(i);
            values[i][0] = Integer.toString(order.getId());
            values[i][1] = order.getSite().getName();
            values[i][2] = order.getProduct().getName();
            values[i][3] = order.getQuantity();
            values[i][4] = order.getUnit().toString();
            values[i][5] = order.getMethod().toString();
            values[i][6] = order.getDateString();
        }
        tableModel = new DefaultTableModel(values, columnNames) {
            @Override
            public Class getColumnClass(int column) {
                if (column == 3) {
                    return Integer.class;
                }
                return String.class;
            }
        };
        table = new JTable(tableModel);
        table.setRowSelectionAllowed(true);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setAutoCreateRowSorter(true);

        scrollPane = new JScrollPane(table);

        add(scrollPane, BorderLayout.CENTER);
        childPanel.add(backBtn);
        childPanel.add(createBtn);
        childPanel.add(deleteBtn);
        childPanel.add(updateBtn);
        add(childPanel, BorderLayout.PAGE_END);
    }

    public JButton getCreateBtn() {
        return createBtn;
    }

    public JButton getDeleteBtn() {
        return deleteBtn;
    }

    public JButton getUpdateBtn() {
        return updateBtn;
    }

    public int getSelectedRowIndex() {
        return table.convertRowIndexToModel(table.getSelectedRow());
    }
}
