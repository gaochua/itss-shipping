package com.bk.view.site.manageSite;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class AddProductFrame extends JFrame {

	private AddProductForm form = new AddProductForm();
	private JButton submitBtn = new JButton("Submit", SitePanel.updateIcon);
	private JButton cancelBtn = new JButton("Cancel", SitePanel.cancelIcon);

	public AddProductFrame() {
		// TODO Auto-generated constructor stub
		setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		this.setTitle("Add New Product");
		this.setMinimumSize(new Dimension(200, 300));
		
		JPanel formPanel = new JPanel();
		formPanel.add(form);
		formPanel.setAlignmentX(CENTER_ALIGNMENT);
		add(formPanel);
		
		JPanel btnsPanel = new JPanel();
		btnsPanel.add(submitBtn);
		btnsPanel.add(cancelBtn);
		add(btnsPanel);
		pack();
	}
	
	public AddProductForm getForm() {
		return form;
	}
	
	public JButton getSubmitBtn() {
		return submitBtn;
	}

	public JButton getCancelBtn() {
		return cancelBtn;
	}

}
