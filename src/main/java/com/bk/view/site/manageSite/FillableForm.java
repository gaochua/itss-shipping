package com.bk.view.site.manageSite;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public abstract class FillableForm extends JPanel {
	
	private List<JComponent> comps = new ArrayList<JComponent>();
	
	public FillableForm() {
		layoutContent();
	}

	public abstract void layoutContent();
	
	public abstract <E> E getContent(int index);
	
	@SuppressWarnings("unchecked")
	public <E> E get(int index) {
		return (E) comps.get(index);
	}
	
	public void add(JComponent c) {
		comps.add(c);
		super.add(c);
	}
	
	public void addAll(JComponent[] a) {
		comps.addAll(Arrays.asList(a));
		for (JComponent c : a) super.add(c);
	}
	
}
