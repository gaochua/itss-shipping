package com.bk.helpers;

import com.bk.utility.HibernateUtil;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.List;

import com.bk.model.User;

public class HibernateUtilTest {
    @Test
    public void get() {
        List<User> results = HibernateUtil.get(session -> session.createQuery("From User").list());
        assertTrue("no users found", results.size() > 0);
    }
}
