package com.bk.controller.order;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import com.bk.model.ImportSite;
import com.bk.model.SalesOrder;
import com.bk.utility.HibernateUtil;
import com.bk.view.site.ChooseSiteStep1Panel;
import com.bk.view.site.ChooseSiteStep2Panel;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ChooseImportSiteTest {
    private SalesOrder salesOrder;
    private List<ImportSite> sites;
    private List<ImportSite> suitableSites;
    private ChooseSiteStep1Panel panel1;
    private ChooseSiteStep2Panel panel2;

    @BeforeClass
    public void beforeChooseImportSiteTest() {
        sites = HibernateUtil.get(session -> session.createQuery("From ImportSite").list());
        List<SalesOrder> salesOrders = HibernateUtil.get(session -> session.createQuery("From SalesOrder").list());
        salesOrder = salesOrders.get(0);
    }

    @Before
    public void before() {
        assertNotNull("Not available sites", sites);
        assertNotNull("Not available sales order", salesOrder);
        panel1 = new ChooseSiteStep1Panel();
        panel1.setSalesOrder(salesOrder);
        panel2 = new ChooseSiteStep2Panel();
        panel2.setSalesOrder(salesOrder);
        panel2.setAllSites(sites);
    }

    @Test
    public void setSalesOrderItems() {
        panel1.setSalesOrderItems();
        panel2.setSalesOrderItems();
        assertNotNull("Error: Not sales order items", panel1.getSalesOrderItems());
        assertNotNull("Error: Not sales order items", panel2.getSalesOrderItems());
    }
   

}