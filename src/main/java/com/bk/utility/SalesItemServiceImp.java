package com.bk.utility;

import java.util.ArrayList;
import java.util.List;

import com.bk.model.SalesOrderItem;

public class SalesItemServiceImp implements ISalesItemService{

	@Override
	public List<SalesOrderItem> getList(int orderId) {
		// TODO Auto-generated method stub
		List<SalesOrderItem> list = new ArrayList<>();
		list = HibernateUtil.get(session -> session.createQuery("From SalesOrderItem Where order.id =: orderId").setParameter("orderId", orderId).list());
//		list = HibernateUtil.get(session -> session.createQuery("Select soi From SalesOrderItem soi Where soi.order.id =: orderId").setParameter("orderId", orderId).list());
		return list;
	}

	
	@Override
	public void create(SalesOrderItem salesItem) {
		// TODO Auto-generated method stub
		 HibernateUtil.process(session -> session.save(salesItem));
	}

	
	@Override
	public void update(SalesOrderItem salesItem) {
		// TODO Auto-generated method stub
		HibernateUtil.process(session -> session.update(salesItem));

	}


	@Override
	public void delete(SalesOrderItem salesItem) {
		// TODO Auto-generated method stub
        HibernateUtil.process(session -> session.delete(salesItem));

	}


}
