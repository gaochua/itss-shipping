package com.bk.model;

public enum Unit {
    KG, BOTTLE, BOX, PACKAGE
}