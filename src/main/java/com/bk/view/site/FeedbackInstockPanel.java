package com.bk.view.site;

import com.bk.model.ImportSite;
import com.bk.model.Product;
import com.bk.model.SalesOrder;
import com.bk.model.SalesOrderItem;
import com.bk.model.SiteInstock;

import jiconfont.icons.font_awesome.FontAwesome;
import jiconfont.swing.IconFontSwing;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FeedbackInstockPanel extends JPanel {
    private JButton exitBtn = new JButton("Exit");
    private JScrollPane scrollPane = new JScrollPane();
    private JPanel childPanel = new JPanel();
    private JTable table;
    private DefaultTableModel tableModel;

    private SalesOrder salesOrder;
    private List<ImportSite> selectedSites;

    public void setSelectedSites(List<ImportSite> selectedSites) {
        this.selectedSites = selectedSites;
        showData();
    }

    public void setSalesOrder(SalesOrder salesOrder) {
        this.salesOrder = salesOrder;
    }

    public SalesOrder getSalesOrder() {
        return this.salesOrder;
    }

    private Set<Product> getOrderProducts() {
        Set<SalesOrderItem> orderItems = getSalesOrder().getItems();
        Set<Product> orderProducts = new HashSet<Product>();
        for(SalesOrderItem orderItem: orderItems) {
            orderProducts.add(orderItem.getProduct());
        }
        return orderProducts;
    }

    public List<SiteInstock> getInstockFromSite(ImportSite site) {
        List<SiteInstock> instocks = new ArrayList<SiteInstock>();
        Set<Product> orderProducts = getOrderProducts();
        for(SiteInstock instock: site.getInstocks()) {
            if(orderProducts.contains(instock.getProduct())) {
                instocks.add(instock);
            }
        }
        return instocks;
    }

    private void setupButtons() {
        Icon exitIcon = IconFontSwing.buildIcon(FontAwesome.TIMES, 16);
        exitBtn.setIcon(exitIcon);
        
    }

    public FeedbackInstockPanel() {
        setLayout(new BorderLayout());
        setupButtons();
    }

    public JButton getExitBtn() {
        return exitBtn;
    }

    public void showData() {
        childPanel.setLayout(new FlowLayout());
        
        remove(scrollPane);
        remove(childPanel);

        int valuesSize = 0;
        for(ImportSite site: selectedSites) {
            valuesSize += getInstockFromSite(site).size();
        }
        
        String[] columnNames = {"Site code", "Merchandise code", "Name", "Quantity", "Unit"};
        String[][] values = new String[valuesSize][5];
        int index = 0;
        for (ImportSite site: selectedSites) {
            for(SiteInstock instock: getInstockFromSite(site)) {
                values[index][0] = Integer.toString(site.getId());
                values[index][1] = Integer.toString(instock.getProduct().getId());
                values[index][2] = instock.getProduct().getName();
                values[index][3] = Integer.toString(instock.getQuantity());
                values[index][4] = instock.getUnit().name();
                index++;
            }
                       
            // for(SiteInstock instock: instocks) {
            //     values[i][1] += "MERCHANDISE CODE: " + instock.getProduct().getId() + "- NAME: " + instock.getProduct().getName()+ " - QUANTITY: " + instock.getQuantity() + " - UNIT: " + instock.getUnit() + "\n";
            // }
            
        }
        tableModel = new DefaultTableModel(values, columnNames);
        table = new JTable(tableModel);
        table.setRowSelectionAllowed(false);
        
        // table.setFont(new Font("Serif", Font.BOLD, 16));

        scrollPane = new JScrollPane(table);

        add(scrollPane, BorderLayout.CENTER);
        childPanel.add(exitBtn);
        
        add(childPanel, BorderLayout.PAGE_END);
    }

}
