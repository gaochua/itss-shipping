package com.bk.model;


import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "delivery_order")
public class DeliveryOrder implements DataModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name="site_id", nullable=false)
    private ImportSite site;

    @ManyToOne
    @JoinColumn(name="product_id", nullable=false)
    private Product product;

    @Column(nullable = false)
    private int quantity;

    @Column(nullable = false)
    private Unit unit;

    @Column
    private Date createdDate = new Date();

    @Column(nullable = false)
    private DeliveryMethod method = DeliveryMethod.AIR;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    @Column(nullable = false)
    private boolean isChecked = false;

    @Column(nullable = false)
    private int quantityReceived = 0;


    public DeliveryOrder(ImportSite site, Product product, int quantity, Unit unit) {
        this.site = site;
        this.product = product;
        this.quantity = quantity;
        this.unit = unit;
    }

    public DeliveryOrder(ImportSite site, Product product, int quantity, Unit unit, DeliveryMethod method) {
        this.site = site;
        this.product = product;
        this.quantity = quantity;
        this.unit = unit;
        this.method = method;
    }

    public DeliveryOrder() {}

    public DeliveryMethod getMethod() {
        return method;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getDateString() {
        return new SimpleDateFormat("dd/mm/yyyy").format(createdDate);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ImportSite getSite() {
        return site;
    }

    public void setSite(ImportSite site) {
        this.site = site;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "DeliveryOrder{" +
                "id=" + id +
                ", site=" + site +
                ", product=" + product +
                ", quantity=" + quantity +
                ", unit=" + unit +
                ", method=" + method +
                '}';
    }

    public void setMethod(DeliveryMethod method) {
        this.method = method;
    }

	public int getQuantityReceived() {
		return quantityReceived;
	}

	public void setQuantityReceived(int quantityReceived) {
		this.quantityReceived = quantityReceived;
	}
    
    
}
