package com.bk.utility;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

import com.bk.model.ImportSite;
import com.bk.model.SiteInstock;

public class ProductTableModel extends AbstractTableModel {
	
	private ImportSite site;
	private String columnNames[] = {"Merchandise Code", "Product", "Description", "Unit", "Quantity", "Select"};
	public static final int MERCHANDISE_CODE_COLUMN = 0;
	public static final int PRODUCT_COLUMN = 1;
	public static final int DESCRIPTION_COLUMN = 2;
	public static final int UNIT_COLUMN = 3;
	public static final int QUANTITY_COLUMN = 4;
	public static final int SELECT_COLUMN = 5;
		
	private List<SiteInstock> productList;
	private List<Boolean> checkList;
	
	public List<SiteInstock> getProductList() {
		return productList;
	}

	public List<Boolean> getCheckList() {
		return checkList;
	}

	public ProductTableModel(ImportSite site) {
		// TODO Auto-generated constructor stub
		this.site = site;
		List<SiteInstock> instocks = HibernateUtil.get(session -> session.createQuery("From SiteInstock Where site.id = :siteid").setParameter("siteid", site.getId()).getResultList());
		productList = new ArrayList<>(instocks);
//		productList.sort(new Comparator<SiteInstock>() {
//			@Override
//			public int compare(SiteInstock o1, SiteInstock o2) {
//				// TODO Auto-generated method stub
//				return o1.getId() < o2.getId() ? -1 : o1.getId() == o2.getId() ? 0 : 1;
//			}
//		});
		checkList = new ArrayList<Boolean>();
		for (int i = 0; i < productList.size(); ++i) {
			checkList.add(false);
		}
	}
	
	@Override
	public int getRowCount() {
		return productList.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }
     
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (productList.isEmpty()) {
            return Object.class;
        }
        return getValueAt(0, columnIndex).getClass();
    }

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		SiteInstock product = productList.get(rowIndex);
		Object returnValue = null;
		switch (columnIndex) {
		case MERCHANDISE_CODE_COLUMN:// ID 
			returnValue = product.getId();
			break;
		case PRODUCT_COLUMN:// Product
			returnValue = product.getProduct().getName();
			break;
		case DESCRIPTION_COLUMN:// Description
			returnValue = product.getProduct().getDescription();
			break;
		case UNIT_COLUMN:// Unit
			returnValue = product.getUnit();
			break;
		case QUANTITY_COLUMN:// Quantity
			returnValue = product.getQuantity();
			break;
		case SELECT_COLUMN:
			returnValue = checkList.get(rowIndex);
		default:
			break;
		}
		return returnValue;
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columnIndex == SELECT_COLUMN;
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (columnIndex == SELECT_COLUMN) {
			checkList.set(rowIndex, (Boolean) aValue);
		}
	}
	
	public List<SiteInstock> getSelectedCheckBoxes() {
		List<SiteInstock> result = new ArrayList<SiteInstock>();
		for (int i = 0; i < getRowCount(); ++i) {
			if (true == (Boolean) getValueAt(i, 5)) {
				result.add(productList.get(i));
			}
		}
		return result;
	}

}
