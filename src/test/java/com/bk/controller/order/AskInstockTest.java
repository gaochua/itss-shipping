package com.bk.controller.order;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import com.bk.model.ImportSite;
import com.bk.model.SalesOrder;
import com.bk.utility.HibernateUtil;
import com.bk.view.order.AskInstockPanel;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AskInstockTest {
    private SalesOrder salesOrder;
    private List<ImportSite> sites;
    private List<ImportSite> suitableSites;
    private AskInstockPanel panel;
    @BeforeClass
    public void beforeAskInstock() {
        sites = HibernateUtil.get(session -> session.createQuery("From ImportSite").list());
        List<SalesOrder> salesOrders = HibernateUtil.get(session -> session.createQuery("From SalesOrder").list());
        salesOrder = salesOrders.get(0);
    }

    @Before
    public void before() {
        assertNotNull("Not available sites", sites);
        assertNotNull("Not available sales order", salesOrder);
        panel = new AskInstockPanel();
        panel.setSalesOrder(salesOrder);
        panel.setAllSites(sites);
    }

    @Test
    public void suitableSitesTest() {
        panel.setSuitableSites();
        suitableSites = panel.getSuitableSites();
        assertNotNull("No sites have order product", suitableSites);
    }

    @Test
    public void instockFromSiteTest() {
        for(ImportSite site: suitableSites) {
            assertNotNull("Error! Not available instock", panel.getInstockFromSite(site));
        }
    }

}