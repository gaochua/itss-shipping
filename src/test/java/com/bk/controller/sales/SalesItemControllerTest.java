package com.bk.controller.sales;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Random;
import java.util.Vector;

import javax.swing.JTextField;

import org.junit.Test;

import com.bk.model.Product;
import com.bk.model.SalesOrder;
import com.bk.model.SalesOrderItem;
import com.bk.utility.IProductService;
import com.bk.utility.ISalesItemService;
import com.bk.utility.ISalesOrderService;
import com.bk.utility.ProductServiceImp;
import com.bk.utility.SalesItemServiceImp;
import com.bk.utility.SalesOrderServiceImp;

public class SalesItemControllerTest {

	@Test
	public void testSetView() {
		IProductService productService= new ProductServiceImp();
		List<Product> listProduct = productService.getList();
		Vector<Product> productVector = new Vector<>(listProduct);

		ISalesItemService salesItemService = new SalesItemServiceImp();
		List<SalesOrderItem> listSalesOrderItem = salesItemService.getList(2);
		
		Random randomGenerator = new Random();
		SalesOrderItem salesItem = listSalesOrderItem.get(randomGenerator.nextInt(listSalesOrderItem.size()));

		JTextField tfSalesItemId = new JTextField();
		JTextField tfProduct = new JTextField();
		
		if (salesItem.getId() == 0) {
			System.out.println("Can not found sales item");

		} else {
			tfSalesItemId.setText(Integer.toString(salesItem.getId()));
			for (Product product : productVector) {
				if (product.equals(salesItem.getProduct())) {
					tfProduct.setText(product.toString());
					break;
				}
			}
		}
		
		assertTrue("No sales item found", tfSalesItemId.getText().length() > 0);
	}
	
}
