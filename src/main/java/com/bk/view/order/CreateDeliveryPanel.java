package com.bk.view.order;


public class CreateDeliveryPanel extends UpdateDeliveryPanel {

    public CreateDeliveryPanel() {
        super();
        getSubmitBtn().setText("Create");
        setTitle("Create order");
    }

}
