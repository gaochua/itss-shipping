package com.bk.utility;

import com.bk.model.*;
import org.hibernate.Session;

import java.util.function.Consumer;

public class DbHelper {
    public static void populateDb() {
    	User sales_user = new User("sales", "sales", Role.SALES_EMPLOYEE);
        User import_site_user = new User("import_site", "import_site", Role.IMPORT_SITE_EMPLOYEE);
        User overseas_order_user = new User("overseas_order", "overseas_order", Role.ORDER_EMPLOYEE);
        User warehouse_user = new User("warehouse", "warehouse", Role.WAREHOUSE_EMPLOYEE);

        Product kitkat = new Product("kitkat", "japanese candy");
        Product chocopie = new Product("chocopie", "korean choco pie delicious");
        Product coca = new Product("cocacola", "carbonated drink");

        SalesOrder order1 = new SalesOrder(import_site_user);
        SalesOrderItem salesOrderItem1 = new SalesOrderItem(kitkat, 4, Unit.BOX, order1);
        SalesOrderItem salesOrderItem2 = new SalesOrderItem(chocopie, 2, Unit.PACKAGE, order1);
        SalesOrderItem salesOrderItem3 = new SalesOrderItem(kitkat, 1, Unit.BOTTLE, order1);

        ImportSite site1 = new ImportSite("tiki", 2, 3);
        ImportSite site2 = new ImportSite("amazon", 3, 2);
        import_site_user.setSite(site1);

        SiteInstock siteInstock1 = new SiteInstock(kitkat, 100, Unit.BOX, site1);
        SiteInstock siteInstock2 = new SiteInstock(chocopie, 200, Unit.PACKAGE, site1);
        SiteInstock siteInstock3 = new SiteInstock(coca, 300, Unit.BOTTLE, site1);
        SiteInstock siteInstock4 = new SiteInstock(kitkat, 400, Unit.BOX, site2);
        SiteInstock siteInstock5 = new SiteInstock(chocopie, 500, Unit.PACKAGE, site2);
        SiteInstock siteInstock6 = new SiteInstock(coca, 600, Unit.BOTTLE, site2);

        DeliveryOrder deliveryOrder1 = new DeliveryOrder(site1, kitkat, 20, Unit.BOX);
        DeliveryOrder deliveryOrder2 = new DeliveryOrder(site2, coca, 10, Unit.BOTTLE);

        HibernateUtil.process(new Consumer<Session>() {
            @Override
            public void accept(Session session) {
                session.save(sales_user);
                session.save(import_site_user);
                session.save(overseas_order_user);
                session.save(warehouse_user);
                session.save(kitkat);
                session.save(chocopie);
                session.save(coca);
                session.save(order1);
                session.save(salesOrderItem1);
                session.save(salesOrderItem2);
                session.save(salesOrderItem3);
                session.save(site1);
                session.save(site2);
                session.save(siteInstock1);
                session.save(siteInstock2);
                session.save(siteInstock3);
                session.save(siteInstock4);
                session.save(siteInstock5);
                session.save(siteInstock6);
                session.save(deliveryOrder1);
                session.save(deliveryOrder2);
                
            }
        });
    }

    public static void main(String[] args) {
        DbHelper.populateDb();
    }
}
