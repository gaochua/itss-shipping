package com.bk.utility;

import java.util.ArrayList;
import java.util.List;

import com.bk.model.Product;

public class ProductServiceImp implements IProductService{

	@Override
	public List<Product> getList() {
		// TODO Auto-generated method stub
		List<Product> list = new ArrayList<>();
		list = HibernateUtil.get(session -> session.createQuery("From Product").list());

		return list;
	}

	@Override
	public Product getById(int productId) {
		// TODO Auto-generated method stub
		List<Product> list = new ArrayList<>();
		list = HibernateUtil.get(session -> session.createQuery("From Product Where id =: productId").setParameter("productId", productId).list());

		return list.get(0);
	}

}
