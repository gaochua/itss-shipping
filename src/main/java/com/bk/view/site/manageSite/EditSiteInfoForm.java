package com.bk.view.site.manageSite;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.bk.controller.site.SiteController;
import com.bk.utility.IntegerFilterDocument;

@SuppressWarnings("serial")
public class EditSiteInfoForm extends FillableForm {

	public EditSiteInfoForm() {
		super();
	}

	@Override
	public void layoutContent() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		JLabel siteNameJLabel = new JLabel("Site Name:");
		JLabel siteDayByAirJLabel = new JLabel("Days delivery by air:");
		JLabel siteDayByShipJLabel = new JLabel("Days delivery by ship:");
		JTextField siteNameField = new JTextField(SiteController.getSite().getName());
		JTextField siteDayByAirField = new JTextField();
		JTextField siteDayByShipField = new JTextField();
		siteDayByAirField.setDocument(new IntegerFilterDocument());
		siteDayByShipField.setDocument(new IntegerFilterDocument());
		siteDayByAirField.setText(Integer.toString(SiteController.getSite().getDays_delivery_air()));
		siteDayByShipField.setText(Integer.toString(SiteController.getSite().getDays_delivery_ship()));
		add(siteNameJLabel);
		add(siteNameField);
		add(siteDayByAirJLabel);
		add(siteDayByAirField);
		add(siteDayByShipJLabel);
		add(siteDayByShipField);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <E> E getContent(int index) {
		if (index % 2 == 0) {
			return (E) ((JLabel) get(index)).getText();
		}
		else {
			if (index == 1) {
				return (E) ((JTextField) get(index)).getText();
			}
			else if (index == 3) {
				return (E) ((Integer) Integer.parseInt(((JTextField) get(index)).getText()));
			}
			else {
				return (E) ((Integer) Integer.parseInt(((JTextField) get(index)).getText()));
			}
		}
	}
}