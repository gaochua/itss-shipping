package com.bk.controller.site;

import javax.swing.JOptionPane;

import com.bk.App;
import com.bk.model.ImportSite;
import com.bk.utility.HibernateUtil;
import com.bk.view.site.manageSite.EditSiteInfoForm;
import com.bk.view.site.manageSite.EditSiteInfoFrame;

public class EditSiteInfoController extends SiteController {

	public EditSiteInfoController() {
		sitePanel.getEditInfoBtn().addActionListener(actionEvent -> {
			editSiteInfoFrame = new EditSiteInfoFrame(site);
        	initEditSiteInfo();
        	editSiteInfoFrame.pack();
        	editSiteInfoFrame.setLocationRelativeTo(App.mainFrame);
        	editSiteInfoFrame.setVisible(true);
        });
	}
	
	private void initEditSiteInfo() {
        editSiteInfoFrame.getCancelBtn().addActionListener(actionEvent -> {
        	editSiteInfoFrame.dispose();
        });
        
        editSiteInfoFrame.getSubmitBtn().addActionListener(actionEvent -> {
        	EditSiteInfoForm form = editSiteInfoFrame.getForm();
        	String newName = form.getContent(1);
        	Integer newAirDays = 0;
			Integer newShipDays = 0;
    		try{
    			newAirDays = form.getContent(3);
    			newShipDays = form.getContent(5);
    		}
    		catch (Exception e) {
    			JOptionPane.showMessageDialog(null, "days can't be empty!", "ERROR", JOptionPane.ERROR_MESSAGE);
				return;
			}
    		
        	int choice = JOptionPane.showConfirmDialog(null, "Confirm update site information?", "UPDATE", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        	if (choice == JOptionPane.YES_OPTION) {
        		editSiteInfo(site, newName, newAirDays, newShipDays);
        		JOptionPane.showMessageDialog(null, "Edit site information successfully!", "SUCCESS", JOptionPane.INFORMATION_MESSAGE);
            	editSiteInfoFrame.dispose();
        	}
        });
	}
	
	public void editSiteInfo(ImportSite site, String newName, Integer newAirDays, Integer newShipDays) {
		site.setName(newName);
		site.setDays_delivery_air(newAirDays);
		site.setDays_delivery_ship(newShipDays);
		HibernateUtil.process(session -> session.update(site));
		sitePanel.getSiteIdLabel().setText("You are logging in as site " + site.getName() + " (ID: " + site.getId() + ")");
		sitePanel.getSiteNameLabel().setText("Site Name: " + site.getName());
		sitePanel.getDaysAirLabel().setText("Delivery days by air: " + site.getDays_delivery_air());
		sitePanel.getDaysShipLabel().setText("Delivery days by ship: " + site.getDays_delivery_ship());
    	sitePanel.updateUI();
	}
}
