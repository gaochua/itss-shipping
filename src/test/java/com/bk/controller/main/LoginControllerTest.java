package com.bk.controller.main;

import com.bk.model.Role;
import org.junit.Test;

import static org.junit.Assert.*;

public class LoginControllerTest {

    @Test
    public void findUser() {
        AbstractLoginController loginController = new LoginController();
        assertNotNull("failed to find user", loginController.findUser(
                "sales", "sales", Role.SALES_EMPLOYEE));
        assertNull("invalid user", loginController.findUser(
                "sales", "invalid", Role.SALES_EMPLOYEE));
    }
}