package com.bk.controller.order;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import com.bk.utility.HibernateUtil;
import com.bk.model.DeliveryOrder;
import com.bk.model.ImportSite;
import com.bk.model.Product;
import com.bk.model.SalesOrder;
import com.bk.model.SalesOrderItem;
import com.bk.model.SiteInstock;

public class OverseasOrderControllerTest {
    @Test
    public void getDeliveryOrders() {
        List<DeliveryOrder> deliveryOrders = HibernateUtil.get(session -> session.createQuery("From DeliveryOrder").list());
        assertTrue("No delivery order found", deliveryOrders.size() > 0);
    }

    @Test
    public void getProducts() {
        List<Product> products = HibernateUtil.get(session -> session.createQuery("From Product").list());
        assertTrue("No product found", products.size() > 0);
    }

    @Test
    public void getSites() {
        List<ImportSite> sites = HibernateUtil.get(session -> session.createQuery("From ImportSite").list());
        assertTrue("No site found", sites.size() > 0);
    }

    @Test
    public void getSalesOrders() {
        List<SalesOrder> salesOrders = HibernateUtil.get(session -> session.createQuery("From SalesOrder").list());
        assertTrue("No sales order found", salesOrders.size() > 0);
    }

    @Test
    public void getInstocks() {
        List<SiteInstock> instocks = HibernateUtil.get(session -> session.createQuery("From SiteInstock").list());
        assertTrue("No instock found", instocks.size() > 0);
    }

    @Test
    public void getSalesOrderItems() {
        List<SalesOrderItem> items = HibernateUtil.get(session -> session.createQuery("From SalesOrderItem").list());
        assertTrue("No sales order item found", items.size() > 0);
    }
}